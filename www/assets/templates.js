define('appkit/templates/action', [], function(){ return Ember.Handlebars.template(function anonymous(Handlebars,depth0,helpers,partials,data) {
this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Ember.Handlebars.helpers); data = data || {};
  var buffer = '', hashTypes, hashContexts, escapeExpression=this.escapeExpression;


  data.buffer.push("<div class='row bottomPad topPad'>\n  <div class=\"col-xs-1\">\n  </div>\n  <div class='col-xs-10 inTheMiddle'>\n    Improve your credit status by following our action plan\n  </div>\n</div>\n\nRight now this is the content we are getting from money smart server.. title and content.. Please put some real stuff in\n\n<div class='row bottomPad topPad'>\n  <div class=\"col-xs-1\">\n  </div>\n  <div class='col-xs-10 inTheMiddle'>\n    <b>");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers._triageMustache.call(depth0, "title", {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("</b>\n  </div>\n</div>\n\n<div class='row bottomPad topPad'>\n  <div class=\"col-xs-1\">\n  </div>\n  <div class='col-xs-10 inTheMiddle'>\n    ");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers._triageMustache.call(depth0, "contentForMonth", {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("\n  </div>\n</div>\n");
  return buffer;
  
}); });

define('appkit/templates/activate', [], function(){ return Ember.Handlebars.template(function anonymous(Handlebars,depth0,helpers,partials,data) {
this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Ember.Handlebars.helpers); data = data || {};
  var buffer = '', stack1, hashContexts, hashTypes, escapeExpression=this.escapeExpression, self=this;

function program1(depth0,data) {
  
  var buffer = '', hashContexts, hashTypes;
  data.buffer.push("\n  <div class=\"row verticalPad\">\n    <div class=\"col-xs-2\"></div>\n    <div ");
  hashContexts = {'class': depth0};
  hashTypes = {'class': "STRING"};
  data.buffer.push(escapeExpression(helpers.bindAttr.call(depth0, {hash:{
    'class': ("errors::hidden :col-xs-8 :redText :inTheMiddle :verticalPad")
  },contexts:[],types:[],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push(">\n      ");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers._triageMustache.call(depth0, "errors", {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("\n    </div>\n  </div>\n");
  return buffer;
  }

  data.buffer.push("<div class=\"row verticalPad whiteBack\">\n  <div class=\"col-xs-1\"></div>\n  <div class='col-xs-10 inTheMiddle'>\n    Activate your credit report\n  </div>\n</div>\n<div class=\"row verticalPad\">\n  <div class=\"col-xs-1\"></div>\n  <div class='col-xs-10 inTheMiddle'>\n    ");
  hashContexts = {'value': depth0,'class': depth0,'placeholder': depth0};
  hashTypes = {'value': "ID",'class': "STRING",'placeholder': "STRING"};
  data.buffer.push(escapeExpression(helpers.view.call(depth0, "Ember.TextField", {hash:{
    'value': ("voucher"),
    'class': ("greenInputField"),
    'placeholder': ("Enter your voucher number here")
  },contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("\n  </div>\n</div>\n<div class=\"row verticalPad\">\n  <div class=\"col-xs-1\"></div>\n  <div class='col-xs-10 inTheMiddle'>\n    ");
  hashContexts = {'value': depth0,'class': depth0,'placeholder': depth0};
  hashTypes = {'value': "ID",'class': "STRING",'placeholder': "STRING"};
  data.buffer.push(escapeExpression(helpers.view.call(depth0, "Ember.TextField", {hash:{
    'value': ("id"),
    'class': ("greenInputField"),
    'placeholder': ("Your ID number here")
  },contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("\n  </div>\n</div>\n\n");
  hashTypes = {};
  hashContexts = {};
  stack1 = helpers['if'].call(depth0, "errors", {hash:{},inverse:self.noop,fn:self.program(1, program1, data),contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data});
  if(stack1 || stack1 === 0) { data.buffer.push(stack1); }
  data.buffer.push("\n\n<div class=\"row inTheMiddle verticalPad\">\n  <div class=\"col-xs-1\"></div>\n  <button ");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers.action.call(depth0, "activate", {hash:{},contexts:[depth0],types:["STRING"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push(" class=\"greenBox greenBack col-xs-10 verticalPad\">Go!</button>\n</div>\n\n");
  return buffer;
  
}); });

define('appkit/templates/application', [], function(){ return Ember.Handlebars.template(function anonymous(Handlebars,depth0,helpers,partials,data) {
this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Ember.Handlebars.helpers); data = data || {};
  var buffer = '', stack1, stack2, hashTypes, hashContexts, options, escapeExpression=this.escapeExpression, self=this, helperMissing=helpers.helperMissing;

function program1(depth0,data) {
  
  
  data.buffer.push("<div class=\"iconContainer inlineBlock inTheMiddle\"><img src=\"assets/img/menuNavIcons/snapshot-white.png\"></div>Snapshot");
  }

function program3(depth0,data) {
  
  
  data.buffer.push("<div class=\"iconContainer inlineBlock inTheMiddle\"><img src=\"assets/img/menuNavIcons/accounts-white.png\"></div>Accounts & Payment history");
  }

function program5(depth0,data) {
  
  
  data.buffer.push("<div class=\"iconContainer inlineBlock inTheMiddle\"><img src=\"assets/img/menuNavIcons/fruad-white.png\"></div>Fraud");
  }

function program7(depth0,data) {
  
  
  data.buffer.push("<div class=\"iconContainer inlineBlock inTheMiddle\"><img src=\"assets/img/menuNavIcons/negative-info-white.png\"></div>Negative Info");
  }

function program9(depth0,data) {
  
  
  data.buffer.push("<div class=\"iconContainer inlineBlock inTheMiddle\"><img src=\"assets/img/menuNavIcons/enquiries-white.png\"></div>Enquiries");
  }

function program11(depth0,data) {
  
  
  data.buffer.push("<div class=\"iconContainer inlineBlock inTheMiddle\"><img src=\"assets/img/menuNavIcons/personal-info-white.png\"></div>Personal Info");
  }

function program13(depth0,data) {
  
  
  data.buffer.push("<div class=\"iconContainer inlineBlock inTheMiddle\"><img src=\"assets/img/menuNavIcons/action-plan-white.png\"></div>Action Plan & Tasks");
  }

function program15(depth0,data) {
  
  
  data.buffer.push("<div class=\"iconContainer inlineBlock inTheMiddle\"><img src=\"assets/img/menuNavIcons/contact-white.png\"></div>Contact");
  }

function program17(depth0,data) {
  
  var buffer = '', stack1, hashTypes, hashContexts;
  data.buffer.push("\n        ");
  hashTypes = {};
  hashContexts = {};
  stack1 = helpers['if'].call(depth0, "backRoute", {hash:{},inverse:self.program(20, program20, data),fn:self.program(18, program18, data),contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data});
  if(stack1 || stack1 === 0) { data.buffer.push(stack1); }
  data.buffer.push("\n      ");
  return buffer;
  }
function program18(depth0,data) {
  
  var buffer = '', hashTypes, hashContexts;
  data.buffer.push("\n          <a href=\"#\" ");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers.action.call(depth0, "gotoBackRoute", {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("><img src=\"assets/img/topNav/back-normal.png\" class=\"navButton\"></a>\n        ");
  return buffer;
  }

function program20(depth0,data) {
  
  var buffer = '', hashTypes, hashContexts;
  data.buffer.push("\n          <a href=\"#\" ");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers.action.call(depth0, "toggleNav", {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("><img src=\"assets/img/topNav/menu-normal.png\" class=\"navButton\"></a>\n        ");
  return buffer;
  }

  data.buffer.push("<nav class=\"pushmenu pushmenu-left ios7statusBar\">\n  ");
  hashTypes = {};
  hashContexts = {};
  options = {hash:{},inverse:self.noop,fn:self.program(1, program1, data),contexts:[depth0],types:["STRING"],hashContexts:hashContexts,hashTypes:hashTypes,data:data};
  stack2 = ((stack1 = helpers['link-to'] || depth0['link-to']),stack1 ? stack1.call(depth0, "report.summary", options) : helperMissing.call(depth0, "link-to", "report.summary", options));
  if(stack2 || stack2 === 0) { data.buffer.push(stack2); }
  data.buffer.push("\n  ");
  hashTypes = {};
  hashContexts = {};
  options = {hash:{},inverse:self.noop,fn:self.program(3, program3, data),contexts:[depth0],types:["STRING"],hashContexts:hashContexts,hashTypes:hashTypes,data:data};
  stack2 = ((stack1 = helpers['link-to'] || depth0['link-to']),stack1 ? stack1.call(depth0, "report.accounts", options) : helperMissing.call(depth0, "link-to", "report.accounts", options));
  if(stack2 || stack2 === 0) { data.buffer.push(stack2); }
  data.buffer.push("\n  ");
  hashTypes = {};
  hashContexts = {};
  options = {hash:{},inverse:self.noop,fn:self.program(5, program5, data),contexts:[depth0],types:["STRING"],hashContexts:hashContexts,hashTypes:hashTypes,data:data};
  stack2 = ((stack1 = helpers['link-to'] || depth0['link-to']),stack1 ? stack1.call(depth0, "report.fraudulent", options) : helperMissing.call(depth0, "link-to", "report.fraudulent", options));
  if(stack2 || stack2 === 0) { data.buffer.push(stack2); }
  data.buffer.push("\n  ");
  hashTypes = {};
  hashContexts = {};
  options = {hash:{},inverse:self.noop,fn:self.program(7, program7, data),contexts:[depth0],types:["STRING"],hashContexts:hashContexts,hashTypes:hashTypes,data:data};
  stack2 = ((stack1 = helpers['link-to'] || depth0['link-to']),stack1 ? stack1.call(depth0, "report.negative", options) : helperMissing.call(depth0, "link-to", "report.negative", options));
  if(stack2 || stack2 === 0) { data.buffer.push(stack2); }
  data.buffer.push("\n  ");
  hashTypes = {};
  hashContexts = {};
  options = {hash:{},inverse:self.noop,fn:self.program(9, program9, data),contexts:[depth0],types:["STRING"],hashContexts:hashContexts,hashTypes:hashTypes,data:data};
  stack2 = ((stack1 = helpers['link-to'] || depth0['link-to']),stack1 ? stack1.call(depth0, "report.enquiries", options) : helperMissing.call(depth0, "link-to", "report.enquiries", options));
  if(stack2 || stack2 === 0) { data.buffer.push(stack2); }
  data.buffer.push("\n  ");
  hashTypes = {};
  hashContexts = {};
  options = {hash:{},inverse:self.noop,fn:self.program(11, program11, data),contexts:[depth0],types:["STRING"],hashContexts:hashContexts,hashTypes:hashTypes,data:data};
  stack2 = ((stack1 = helpers['link-to'] || depth0['link-to']),stack1 ? stack1.call(depth0, "report.profile", options) : helperMissing.call(depth0, "link-to", "report.profile", options));
  if(stack2 || stack2 === 0) { data.buffer.push(stack2); }
  data.buffer.push("\n\n  ");
  hashTypes = {};
  hashContexts = {};
  options = {hash:{},inverse:self.noop,fn:self.program(13, program13, data),contexts:[depth0],types:["STRING"],hashContexts:hashContexts,hashTypes:hashTypes,data:data};
  stack2 = ((stack1 = helpers['link-to'] || depth0['link-to']),stack1 ? stack1.call(depth0, "action", options) : helperMissing.call(depth0, "link-to", "action", options));
  if(stack2 || stack2 === 0) { data.buffer.push(stack2); }
  data.buffer.push("\n  ");
  hashTypes = {};
  hashContexts = {};
  options = {hash:{},inverse:self.noop,fn:self.program(15, program15, data),contexts:[depth0],types:["STRING"],hashContexts:hashContexts,hashTypes:hashTypes,data:data};
  stack2 = ((stack1 = helpers['link-to'] || depth0['link-to']),stack1 ? stack1.call(depth0, "contact", options) : helperMissing.call(depth0, "link-to", "contact", options));
  if(stack2 || stack2 === 0) { data.buffer.push(stack2); }
  data.buffer.push("\n</nav>\n<div class=\"container\">\n  <div id='navBar' class=\"row lightGreyBack greyBottomBorder\">\n    <div class='col-xs-12'>\n      <section class=\"ios7statusBar lightGreyBack\">\n      </section>\n    </div>\n\n    <section class=\"buttonset\">\n      <div class='col-xs-2'>\n      ");
  hashTypes = {};
  hashContexts = {};
  stack2 = helpers['if'].call(depth0, "passcodeVerified", {hash:{},inverse:self.noop,fn:self.program(17, program17, data),contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data});
  if(stack2 || stack2 === 0) { data.buffer.push(stack2); }
  data.buffer.push("\n      </div>\n      <div class='col-xs-8 inTheMiddle'>\n        <img ");
  hashContexts = {'src': depth0};
  hashTypes = {'src': "ID"};
  options = {hash:{
    'src': ("navImage")
  },contexts:[],types:[],hashContexts:hashContexts,hashTypes:hashTypes,data:data};
  data.buffer.push(escapeExpression(((stack1 = helpers['bind-attr'] || depth0['bind-attr']),stack1 ? stack1.call(depth0, options) : helperMissing.call(depth0, "bind-attr", options))));
  data.buffer.push(" class=\"navBarImage\"> ");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers._triageMustache.call(depth0, "navLocation", {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("\n      </div>\n      <div class='col-xs-2'>\n        <!-- Button trigger modal -->\n        <button class=\"infoButtonInNavBar\" data-toggle=\"modal\" data-target=\"#myModal\"></button>\n      </div>\n\n    </section>\n\n  </div>\n  <div class=\"navBarSpacer\"></div>\n  ");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers._triageMustache.call(depth0, "outlet", {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("\n</div>\n\n");
  return buffer;
  
}); });

define('appkit/templates/component-test', [], function(){ return Ember.Handlebars.template(function anonymous(Handlebars,depth0,helpers,partials,data) {
this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Ember.Handlebars.helpers); data = data || {};
  var buffer = '', stack1, hashTypes, hashContexts, helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression, self=this;

function program1(depth0,data) {
  
  var buffer = '', stack1, hashContexts, hashTypes, options;
  data.buffer.push("\n  ");
  hashContexts = {'name': depth0};
  hashTypes = {'name': "ID"};
  options = {hash:{
    'name': ("")
  },contexts:[],types:[],hashContexts:hashContexts,hashTypes:hashTypes,data:data};
  data.buffer.push(escapeExpression(((stack1 = helpers['pretty-color'] || depth0['pretty-color']),stack1 ? stack1.call(depth0, options) : helperMissing.call(depth0, "pretty-color", options))));
  data.buffer.push("\n");
  return buffer;
  }

  hashTypes = {};
  hashContexts = {};
  stack1 = helpers.each.call(depth0, {hash:{},inverse:self.noop,fn:self.program(1, program1, data),contexts:[],types:[],hashContexts:hashContexts,hashTypes:hashTypes,data:data});
  if(stack1 || stack1 === 0) { data.buffer.push(stack1); }
  data.buffer.push("\n");
  return buffer;
  
}); });

define('appkit/templates/components/pretty-color', [], function(){ return Ember.Handlebars.template(function anonymous(Handlebars,depth0,helpers,partials,data) {
this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Ember.Handlebars.helpers); data = data || {};
  var buffer = '', hashTypes, hashContexts, escapeExpression=this.escapeExpression;


  data.buffer.push("Pretty Color: ");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers._triageMustache.call(depth0, "name", {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("\n");
  return buffer;
  
}); });

define('appkit/templates/contact', [], function(){ return Ember.Handlebars.template(function anonymous(Handlebars,depth0,helpers,partials,data) {
this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Ember.Handlebars.helpers); data = data || {};
  var buffer = '', stack1, stack2, hashTypes, hashContexts, options, self=this, helperMissing=helpers.helperMissing;

function program1(depth0,data) {
  
  
  data.buffer.push("Help");
  }

  data.buffer.push("<div class='row bottomPad topPad'>\n  <div class=\"col-xs-1\">\n  </div>\n  <div class='col-xs-10 inTheMiddle'>\n    We want to hear from you\n  </div>\n</div>\n\n<div class='row verticalPad'>\n  <div class='col-xs-1'></div>\n  <div class='greenBox greenBack col-xs-10 verticalPad'>\n    <div clas=\"row\">\n      <div class=\"col-xs-1\">\n      </div>\n      <div class=\"col-xs-2\">\n        <img src=\"assets/img/contact/email.png\" class=\"singleHeight\">\n      </div>\n      <div class=\"col-xs-9\">\n        <a href=\"mailto:Feedback@creditcrunchapp.co.za?subject=Credit%20Crunch%20Feedback&body=Please%20tell%20us%20what%20you%20think%20of%20Credit%20Crunch\" target=\"_blank\" class=\"whiteText\">Send us feedback</a>\n      </div>\n    </div>\n  </div>\n</div>\n<!--<div class='row verticalPad'>-->\n  <!--<div class='col-xs-1'></div>-->\n  <!--<div class='greenBox greenBack col-xs-10 verticalPad'>-->\n    <!--<div clas=\"row whiteText\">-->\n      <!--<div class=\"col-xs-1\">-->\n      <!--</div>-->\n      <!--<div class=\"col-xs-2\">-->\n        <!--<img src=\"assets/img/contact/help.png\" class=\"singleHeight\">-->\n      <!--</div>-->\n      <!--<div class=\"col-xs-9\">-->\n        <!--");
  hashTypes = {};
  hashContexts = {};
  options = {hash:{},inverse:self.noop,fn:self.program(1, program1, data),contexts:[depth0],types:["STRING"],hashContexts:hashContexts,hashTypes:hashTypes,data:data};
  stack2 = ((stack1 = helpers['link-to'] || depth0['link-to']),stack1 ? stack1.call(depth0, "help", options) : helperMissing.call(depth0, "link-to", "help", options));
  if(stack2 || stack2 === 0) { data.buffer.push(stack2); }
  data.buffer.push("-->\n      <!--</div>-->\n    <!--</div>-->\n  <!--</div>-->\n<!--</div>-->\n<div class='row verticalPad'>\n  <div class='col-xs-1'></div>\n  <div class='greenBox greenBack col-xs-10 verticalPad'>\n    <div clas=\"row\">\n      <div class=\"col-xs-1\">\n      </div>\n      <div class=\"col-xs-2\">\n        <img src=\"assets/img/contact/email.png\" class=\"singleHeight\">\n      </div>\n      <div class=\"col-xs-9\">\n        <a href=\"http://www.creditcrunchapp.co.za\" target=\"_blank\" class=\"whiteText\">Visit our website</a>\n      </div>\n    </div>\n  </div>\n</div>\n<div class='row verticalPad'>\n  <div class='col-xs-1'></div>\n  <div class='greenBox greenBack col-xs-10 verticalPad'>\n    <div clas=\"row\">\n      <div class=\"col-xs-1\">\n      </div>\n      <div class=\"col-xs-2\">\n        <img src=\"assets/img/contact/twitter.png\" class=\"singleHeight\">\n      </div>\n      <div class=\"col-xs-9\">\n        <a href=\"#\" onclick=\"window.open('https://twitter.com/intent/user?screen_name=@CreditCrunchSA', '_system', 'location=yes');\" class=\"whiteText\">Follow us on Twitter</a>\n      </div>\n    </div>\n  </div>\n</div>\n<div class='row verticalPad'>\n  <div class='col-xs-1'></div>\n  <div class='greenBox greenBack col-xs-10 verticalPad'>\n    <div clas=\"row\">\n      <div class=\"col-xs-1\">\n      </div>\n      <div class=\"col-xs-2\">\n        <img src=\"assets/img/contact/facebook.png\" class=\"singleHeight\">\n      </div>\n      <div class=\"col-xs-9\">\n        <a href=\"https://www.facebook.com/CreditCrunch\" target=\"_blank\" class=\"whiteText\">Like us on Facebook</a>\n      </div>\n    </div>\n  </div>\n</div>\n\n");
  return buffer;
  
}); });

define('appkit/templates/help', [], function(){ return Ember.Handlebars.template(function anonymous(Handlebars,depth0,helpers,partials,data) {
this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Ember.Handlebars.helpers); data = data || {};
  


  data.buffer.push("<div class='row bottomPad topPad'>\n  <div class=\"col-xs-1\">\n  </div>\n  <div class='col-xs-10 inTheMiddle'>\n    Help\n  </div>\n</div>\n\n");
  
}); });

define('appkit/templates/helper-test', [], function(){ return Ember.Handlebars.template(function anonymous(Handlebars,depth0,helpers,partials,data) {
this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Ember.Handlebars.helpers); data = data || {};
  var buffer = '', stack1, hashTypes, hashContexts, options, helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;


  data.buffer.push("<h3>My name is ");
  hashTypes = {};
  hashContexts = {};
  options = {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data};
  data.buffer.push(escapeExpression(((stack1 = helpers['reverse-word'] || depth0['reverse-word']),stack1 ? stack1.call(depth0, "name", options) : helperMissing.call(depth0, "reverse-word", "name", options))));
  data.buffer.push(".</h3>\n");
  return buffer;
  
}); });

define('appkit/templates/passcode', [], function(){ return Ember.Handlebars.template(function anonymous(Handlebars,depth0,helpers,partials,data) {
this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Ember.Handlebars.helpers); data = data || {};
  var buffer = '', stack1, hashContexts, hashTypes, escapeExpression=this.escapeExpression, self=this;

function program1(depth0,data) {
  
  
  data.buffer.push("<br>");
  }

  data.buffer.push("<div class=\"row bottomPad topPad dblBottomPad whiteBack\">\n  <div class=\"col-xs-1\"></div>\n  <div class='col-xs-10 inTheMiddle'>\n    Create a pass code\n  </div>\n</div>\n<div class=\"row dblBottomPad\">\n  <div class=\"col-xs-1\"></div>\n  <div class='col-xs-10 inTheMiddle'>\n    ");
  hashContexts = {'type': depth0,'value': depth0,'class': depth0,'placeholder': depth0};
  hashTypes = {'type': "STRING",'value': "ID",'class': "STRING",'placeholder': "STRING"};
  data.buffer.push(escapeExpression(helpers.view.call(depth0, "Ember.TextField", {hash:{
    'type': ("tel"),
    'value': ("passcode"),
    'class': ("greenInputField"),
    'placeholder': ("Create a pass code")
  },contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("\n  </div>\n</div>\n<div class=\"row dblBottomPad\">\n  <div class=\"col-xs-1\"></div>\n  <div class='col-xs-10 inTheMiddle'>\n    ");
  hashContexts = {'type': depth0,'value': depth0,'class': depth0,'placeholder': depth0};
  hashTypes = {'type': "STRING",'value': "ID",'class': "STRING",'placeholder': "STRING"};
  data.buffer.push(escapeExpression(helpers.view.call(depth0, "Ember.TextField", {hash:{
    'type': ("tel"),
    'value': ("confirmPasscode"),
    'class': ("greenInputField"),
    'placeholder': ("Confirm pass code")
  },contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("\n  </div>\n</div>\n<div class=\"row dblBottomPad\">\n  <div class=\"col-xs-2\"></div>\n  <div ");
  hashContexts = {'class': depth0};
  hashTypes = {'class': "STRING"};
  data.buffer.push(escapeExpression(helpers.bindAttr.call(depth0, {hash:{
    'class': ("hasErrors::hidden :col-xs-8 :redBox :redBack :inTheMiddle :verticalPad")
  },contexts:[],types:[],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push(">\n    ");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers._triageMustache.call(depth0, "errors.length", {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("\n    ");
  hashTypes = {};
  hashContexts = {};
  stack1 = helpers['if'].call(depth0, "errors.length", {hash:{},inverse:self.noop,fn:self.program(1, program1, data),contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data});
  if(stack1 || stack1 === 0) { data.buffer.push(stack1); }
  data.buffer.push("\n    ");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers._triageMustache.call(depth0, "errors.mismatch", {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("\n  </div>\n</div>\n\n<div class=\"row inTheMiddle dblBottomPad\">\n  <div class=\"col-xs-1\"></div>\n  <button ");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers.action.call(depth0, "verifyAndCreatePasscode", {hash:{},contexts:[depth0],types:["STRING"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push(" class=\"greenBox greenBack col-xs-10 verticalPad\">Go!</button>\n</div>");
  return buffer;
  
}); });

define('appkit/templates/passcode_login', [], function(){ return Ember.Handlebars.template(function anonymous(Handlebars,depth0,helpers,partials,data) {
this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Ember.Handlebars.helpers); data = data || {};
  var buffer = '', hashTypes, hashContexts, escapeExpression=this.escapeExpression;


  data.buffer.push("<div class=\"row dblTopPad dblBottomPad greenBack\">\n  <div class=\"col-xs-1\"></div>\n  <div class='col-xs-10 inTheMiddle'>\n    Welcome back ");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers._triageMustache.call(depth0, "content.firstName", {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("!\n  </div>\n</div>\n<!--<div class=\"row topPad dblBottomPad greenBack\">-->\n  <!--<div class=\"col-xs-1\"></div>-->\n  <!--<div class='col-xs-10 textAlignCenter'>-->\n    <!--Enter your pass code-->\n  <!--</div>-->\n<!--</div>-->\n<div class=\"row verticalPad greenBack\">\n  <div class=\"col-xs-1\"></div>\n  <div class='col-xs-10 textAlignCenter'>\n    ");
  hashContexts = {'type': depth0,'value': depth0,'class': depth0,'placeholder': depth0};
  hashTypes = {'type': "STRING",'value': "ID",'class': "STRING",'placeholder': "STRING"};
  data.buffer.push(escapeExpression(helpers.view.call(depth0, "Ember.TextField", {hash:{
    'type': ("tel"),
    'value': ("passcode"),
    'class': ("greenInputField"),
    'placeholder': ("Enter pass code")
  },contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("\n  </div>\n</div>\n<div class=\"row bottomPad greenBack\">\n  <div class=\"col-xs-2\"></div>\n  <div ");
  hashContexts = {'class': depth0};
  hashTypes = {'class': "STRING"};
  data.buffer.push(escapeExpression(helpers.bindAttr.call(depth0, {hash:{
    'class': ("errors::hidden :col-xs-8 :redText :textAlignCenter :verticalPad")
  },contexts:[],types:[],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push(">\n    ");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers._triageMustache.call(depth0, "errors", {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("\n  </div>\n</div>\n\n<div class=\"row verticalPad greenBack\">\n  <div class=\"col-xs-1\"></div>\n  <div class=\"col-xs-10 inTheMiddle\">\n    <button ");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers.action.call(depth0, "verifyPasscode", {hash:{},contexts:[depth0],types:["STRING"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push(" class=\"greyBox greyBack fullWidth verticalPad\">Go!</button>\n  </div>\n</div>\n\n<div class=\"row verticalPad greenBack\">\n  <div class=\"col-xs-1\"></div>\n  <div class=\"col-xs-10 inTheMiddle\">\n    <a href=\"#\" class=\"whiteText verticalPad\" data-toggle=\"modal\" data-target=\"#forgottenPasscodeModal\">Forgotten your passcode?</a>\n  </div>\n</div>\n\n<br><br>\n\n<div class=\"row bottomPad topPad dblBottomPad greenBack\">\n  <div class=\"col-xs-3\"></div>\n  <div class='col-xs-6 inTheMiddle'>\n    <img src=\"assets/img/firstTimeJourneyAndLogin/green-icon-credit-crunch.png\" class=\"fullWidth\">\n  </div>\n</div>\n\n<div class=\"extendToScreenHeight greenBack\">\n</div>\n\n<!-- Forgotten passcode Modal -->\n<div class=\"modal fade\" id=\"forgottenPasscodeModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">\n  <div class=\"modal-dialog\">\n    <div class=\"modal-content\">\n      <div class=\"modal-header\">\n        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>\n        <h4 class=\"modal-title\" id=\"myModalLabel\">Forgotten your passcode?</h4>\n      </div>\n      <div class=\"modal-body\">\n        Don't worry if you've forgotten your passcode. You can clear it using the button below, but you'll have to get another voucher to enable Credit Crunch on your phone.<br><br>\n\n        You won't be able to undo this!\n      </div>\n      <div class=\"modal-footer\">\n        <button ");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers.action.call(depth0, "clearPasscode", {hash:{},contexts:[depth0],types:["STRING"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push(" type=\"button\" class=\"redBox redBack\" data-dismiss=\"modal\">Clear passcode</button>\n      </div>\n    </div>\n  </div>\n</div>\n\n<!--More info modal-->\n<div class=\"modal fade\" id=\"myModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">\n  <div class=\"modal-dialog\">\n    <div class=\"modal-content\">\n      <div class=\"modal-header\">\n        &nbsp;\n        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>\n      </div>\n      <div class=\"modal-body\">\n        Before you can access Credit Crunch you need to type in the passcode that you registered when you first enabled Credit Crunch on your phone.<br><br>\n\n        If you've forgotten your passcode, you can tap the <i>Forgotten your passcode?</i> button, which will allow you to clear the passcode on this phone. However you will need to get another voucher to re-enable Credit Crunch.\n      </div>\n    </div>\n  </div>\n</div>\n\n");
  return buffer;
  
}); });

define('appkit/templates/report', [], function(){ return Ember.Handlebars.template(function anonymous(Handlebars,depth0,helpers,partials,data) {
this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Ember.Handlebars.helpers); data = data || {};
  var hashTypes, hashContexts, escapeExpression=this.escapeExpression;


  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers._triageMustache.call(depth0, "outlet", {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  
}); });

define('appkit/templates/report/account', [], function(){ return Ember.Handlebars.template(function anonymous(Handlebars,depth0,helpers,partials,data) {
this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Ember.Handlebars.helpers); data = data || {};
  var buffer = '', stack1, stack2, hashTypes, hashContexts, options, helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression, self=this;

function program1(depth0,data) {
  
  var buffer = '', stack1, hashTypes, hashContexts, options;
  data.buffer.push("\n  <div class='col-xs-2 clearBootstrapPadding inTheMiddle lightGreyBorder verticalPad'>\n    ");
  hashTypes = {};
  hashContexts = {};
  options = {hash:{},contexts:[depth0,depth0],types:["ID","ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data};
  data.buffer.push(escapeExpression(((stack1 = helpers.paymentHistoryImg || depth0.paymentHistoryImg),stack1 ? stack1.call(depth0, "", "month", options) : helperMissing.call(depth0, "paymentHistoryImg", "", "month", options))));
  data.buffer.push("\n  </div>\n");
  return buffer;
  }

  data.buffer.push("<div class='row whiteBack verticalPad'>\n  <div class=\"col-xs-1\"></div>\n  <div class='col-xs-10 inTheMiddle'>\n    Your account & payment history for:<br>\n    <span class='orangeText biggerText'>");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers._triageMustache.call(depth0, "subscriber_name", {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("</span>\n  </div>\n</div>\n<div class='row dblBottomPad whiteBack'>\n  <div class='col-xs-4 smallBootstrapPadding'>\n      <div class=\"orangeText orangeBox bottomPad sameHeightBox ninetyWide inTheMiddle\">\n        Current Balance:<br>\n        ");
  hashTypes = {};
  hashContexts = {};
  options = {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data};
  data.buffer.push(escapeExpression(((stack1 = helpers.number || depth0.number),stack1 ? stack1.call(depth0, "current_balance", options) : helperMissing.call(depth0, "number", "current_balance", options))));
  data.buffer.push("\n      </div>\n  </div>\n  <div class='col-xs-4 smallBootstrapPadding'>\n      <div class=\"greenBox blueText bottomPad sameHeightBox ninetyWide inTheMiddle\">\n        Installment:<br>\n        ");
  hashTypes = {};
  hashContexts = {};
  options = {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data};
  data.buffer.push(escapeExpression(((stack1 = helpers.number || depth0.number),stack1 ? stack1.call(depth0, "instalment_amount", options) : helperMissing.call(depth0, "number", "instalment_amount", options))));
  data.buffer.push("\n      </div>\n  </div>\n  <div class='col-xs-4 smallBootstrapPadding'>\n      <div class=\"redText redBox bottomPad sameHeightBox ninetyWide inTheMiddle\">\n        Arrears Amount:<br>\n        ");
  hashTypes = {};
  hashContexts = {};
  options = {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data};
  data.buffer.push(escapeExpression(((stack1 = helpers.number || depth0.number),stack1 ? stack1.call(depth0, "arrears_amount", options) : helperMissing.call(depth0, "number", "arrears_amount", options))));
  data.buffer.push("\n      </div>\n  </div>\n</div>\n<div class='row dottedBottom dottedTop topPad bottomPad whiteBack'>\n  <div class='col-xs-8 dottedRight accountDetailsColumn'>\n    Account details <br>\n    <b>Account number:</b> ");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers.unbound.call(depth0, "account_number", {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("<br>\n    <b>Type of account:</b> ");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers._triageMustache.call(depth0, "accountType", {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("<br>\n    <b>Date opened:</b> ");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers._triageMustache.call(depth0, "created", {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("<br>\n    <b>Opening balance / Credit limit:</b> ");
  hashTypes = {};
  hashContexts = {};
  options = {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data};
  data.buffer.push(escapeExpression(((stack1 = helpers.number || depth0.number),stack1 ? stack1.call(depth0, "credit_limit", options) : helperMissing.call(depth0, "number", "credit_limit", options))));
  data.buffer.push("<br>\n    <b>Last paid:</b> ");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers._triageMustache.call(depth0, "last_payment", {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("<br>\n  </div>\n\n  <div class='col-xs-4 inTheMiddle accountDetailsColumn'>\n    <div ");
  hashContexts = {'class': depth0};
  hashTypes = {'class': "STRING"};
  options = {hash:{
    'class': ("inArrears :fullHeight")
  },contexts:[],types:[],hashContexts:hashContexts,hashTypes:hashTypes,data:data};
  data.buffer.push(escapeExpression(((stack1 = helpers['bind-attr'] || depth0['bind-attr']),stack1 ? stack1.call(depth0, options) : helperMissing.call(depth0, "bind-attr", options))));
  data.buffer.push(">\n      Current status<br><br>\n      ");
  hashTypes = {};
  hashContexts = {};
  options = {hash:{},contexts:[depth0,depth0],types:["ID","STRING"],hashContexts:hashContexts,hashTypes:hashTypes,data:data};
  data.buffer.push(escapeExpression(((stack1 = helpers.currentMonthPaymentIcon || depth0.currentMonthPaymentIcon),stack1 ? stack1.call(depth0, "", "01", options) : helperMissing.call(depth0, "currentMonthPaymentIcon", "", "01", options))));
  data.buffer.push("\n    </div>\n  </div>\n</div>\n\n<div class='row verticalPad whiteBack'>\n  <div class='col-xs-12'>\n    6 months payment history<br>\n    <span class=\"redText\">Tap the icon to see its meaning</span>\n  </div>\n</div>\n<div class='whiteBack'>\n");
  hashTypes = {};
  hashContexts = {};
  stack2 = helpers.each.call(depth0, "month", "in", "months", {hash:{},inverse:self.noop,fn:self.program(1, program1, data),contexts:[depth0,depth0,depth0],types:["ID","ID","ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data});
  if(stack2 || stack2 === 0) { data.buffer.push(stack2); }
  data.buffer.push("\n</div>\n\n<div class=\"modal fade\" id=\"myModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">\n  <div class=\"modal-dialog\">\n    <div class=\"modal-content\">\n      <div class=\"modal-header\">\n        &nbsp;\n        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>\n      </div>\n      <div class=\"modal-body\">\n        Accounts in arrears? Make payment if your account is in arrears or alternatively contact the\n        credit provider to inform them of your situation and try to negotiate better repayment terms.\n        <br><br>\n        If you want to dispute any of the information in your credit report contact XDS (0860 937 000)\n      </div>\n    </div>\n  </div>\n</div>");
  return buffer;
  
}); });

define('appkit/templates/report/accounts', [], function(){ return Ember.Handlebars.template(function anonymous(Handlebars,depth0,helpers,partials,data) {
this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Ember.Handlebars.helpers); data = data || {};
  var buffer = '', stack1, hashTypes, hashContexts, escapeExpression=this.escapeExpression, helperMissing=helpers.helperMissing, self=this;

function program1(depth0,data) {
  
  var buffer = '', stack1, hashContexts, hashTypes;
  data.buffer.push("\n    <div class='col-xs-12 inTheMiddle topPad'>\n      Loan Credit accounts\n    </div>\n\n    ");
  hashContexts = {'itemController': depth0};
  hashTypes = {'itemController': "STRING"};
  stack1 = helpers.each.call(depth0, "loan_accounts", {hash:{
    'itemController': ("account")
  },inverse:self.noop,fn:self.program(2, program2, data),contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data});
  if(stack1 || stack1 === 0) { data.buffer.push(stack1); }
  data.buffer.push("\n  ");
  return buffer;
  }
function program2(depth0,data) {
  
  var buffer = '', stack1, stack2, hashTypes, hashContexts, options;
  data.buffer.push("\n      <div class='col-xs-4 inTheMiddle smallBootstrapPadding verticalPad'>\n        ");
  hashTypes = {};
  hashContexts = {};
  options = {hash:{},inverse:self.noop,fn:self.program(3, program3, data),contexts:[depth0,depth0],types:["STRING","ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data};
  stack2 = ((stack1 = helpers['link-to'] || depth0['link-to']),stack1 ? stack1.call(depth0, "report.account", "", options) : helperMissing.call(depth0, "link-to", "report.account", "", options));
  if(stack2 || stack2 === 0) { data.buffer.push(stack2); }
  data.buffer.push("\n      </div>\n    ");
  return buffer;
  }
function program3(depth0,data) {
  
  var buffer = '', stack1, hashTypes, hashContexts, options;
  data.buffer.push("\n          <div class=\"orangeBox bottomPad sameHeightBox\">\n            <img src='");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers.unbound.call(depth0, "accountStatusImgSrc", {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("' class=\"singleHeight\"><br>\n            ");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers._triageMustache.call(depth0, "subscriber_name", {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("\n            <div class=\"orangeBox orangeBack inTheMiddle ninetyWide\">\n              ");
  hashTypes = {};
  hashContexts = {};
  options = {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data};
  data.buffer.push(escapeExpression(((stack1 = helpers.number || depth0.number),stack1 ? stack1.call(depth0, "current_balance", options) : helperMissing.call(depth0, "number", "current_balance", options))));
  data.buffer.push("\n            </div>\n          </div>\n        ");
  return buffer;
  }

function program5(depth0,data) {
  
  var buffer = '', stack1, hashContexts, hashTypes;
  data.buffer.push("\n    <div class='col-xs-12 inTheMiddle'>\n      Credit accounts\n    </div>\n\n    ");
  hashContexts = {'itemController': depth0};
  hashTypes = {'itemController': "STRING"};
  stack1 = helpers.each.call(depth0, "consumer_accounts", {hash:{
    'itemController': ("account")
  },inverse:self.noop,fn:self.program(6, program6, data),contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data});
  if(stack1 || stack1 === 0) { data.buffer.push(stack1); }
  data.buffer.push("\n  ");
  return buffer;
  }
function program6(depth0,data) {
  
  var buffer = '', stack1, stack2, hashTypes, hashContexts, options;
  data.buffer.push("\n      <div class='col-xs-4 inTheMiddle equalHeightCell smallBootstrapPadding verticalPad'>\n        ");
  hashTypes = {};
  hashContexts = {};
  options = {hash:{},inverse:self.noop,fn:self.program(7, program7, data),contexts:[depth0,depth0],types:["STRING","ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data};
  stack2 = ((stack1 = helpers['link-to'] || depth0['link-to']),stack1 ? stack1.call(depth0, "report.account", "", options) : helperMissing.call(depth0, "link-to", "report.account", "", options));
  if(stack2 || stack2 === 0) { data.buffer.push(stack2); }
  data.buffer.push("\n      </div>\n    ");
  return buffer;
  }
function program7(depth0,data) {
  
  var buffer = '', stack1, hashTypes, hashContexts, options;
  data.buffer.push("\n          <div class=\"blueBox bottomPad sameHeightBox\">\n            <img src='");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers.unbound.call(depth0, "accountStatusImgSrc", {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("' class=\"singleHeight\"><br>\n            ");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers._triageMustache.call(depth0, "subscriber_name", {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("\n            <div class=\"blueBox blueBack inTheMiddle ninetyWide\">\n              ");
  hashTypes = {};
  hashContexts = {};
  options = {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data};
  data.buffer.push(escapeExpression(((stack1 = helpers.number || depth0.number),stack1 ? stack1.call(depth0, "current_balance", options) : helperMissing.call(depth0, "number", "current_balance", options))));
  data.buffer.push("\n            </div>\n          </div>\n        ");
  return buffer;
  }

  data.buffer.push("<div class=\"row bottomPad topPad whiteBack\">\n  <div class='col-xs-12 inTheMiddle'>\n    Choose the account you want to view\n  </div>\n</div>\n<div class='row greyTopBorder lightGreyBack'>\n  ");
  hashTypes = {};
  hashContexts = {};
  stack1 = helpers['if'].call(depth0, "showLoanAccounts", {hash:{},inverse:self.noop,fn:self.program(1, program1, data),contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data});
  if(stack1 || stack1 === 0) { data.buffer.push(stack1); }
  data.buffer.push("\n</div>\n<div class='row lightGreyBack topPad'>\n  ");
  hashTypes = {};
  hashContexts = {};
  stack1 = helpers['if'].call(depth0, "showCreditAccounts", {hash:{},inverse:self.noop,fn:self.program(5, program5, data),contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data});
  if(stack1 || stack1 === 0) { data.buffer.push(stack1); }
  data.buffer.push("\n</div>\n\n<!-- Modal -->\n<div class=\"modal fade\" id=\"myModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">\n  <div class=\"modal-dialog\">\n    <div class=\"modal-content\">\n      <div class=\"modal-header\">\n        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>\n        <h4 class=\"modal-title\" id=\"myModalLabel\">Your Accounts</h4>\n      </div>\n      <div class=\"modal-body\">\n        Here you’ll see all your accounts and how you’ve managed your debt repayments. This is an overview of your payment behaviour over the last 24 months.<br><br>\n\n        Click on each account to see how you’ve managed your debt repayments:\n        <ul>\n          <li>See monthly instalments</li>\n          <li>Know total debt balances</li>\n          <li>View outstanding payments</li>\n          <li>Get your settlement amounts</li>\n        </ul>\n\n        Accounts in arrears? Make payment on your\n        account(s) in arrears or alternatively contact the\n        credit provider to inform them of your situation\n        and try to negotiate better repayment terms.\n      </div>\n    </div>\n  </div>\n</div>\n\n");
  return buffer;
  
}); });

define('appkit/templates/report/admin_orders', [], function(){ return Ember.Handlebars.template(function anonymous(Handlebars,depth0,helpers,partials,data) {
this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Ember.Handlebars.helpers); data = data || {};
  var buffer = '', stack1, hashTypes, hashContexts, escapeExpression=this.escapeExpression, self=this;

function program1(depth0,data) {
  
  var buffer = '', hashTypes, hashContexts;
  data.buffer.push("\n  <div class=\"row whiteBack verticalPad\">\n    <div class=\"col-xs-1\"></div>\n    <div class=\"col-xs-11 bottomPad\">\n      Case no: <span class='lightText'>");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers.unbound.call(depth0, "case_number", {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("</span><br>\n      Issue Date: <span class='lightText'>");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers.unbound.call(depth0, "filing_date", {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("</span><br>\n      Judgement Type: <span class='lightText'>");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers.unbound.call(depth0, "case_type", {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("</span><br>\n      Amount: <span class='lightText'>");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers.unbound.call(depth0, "amount", {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("</span><br>\n      Plaintiff: <span class='lightText'>");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers.unbound.call(depth0, "plaintiff_name", {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("</span><br>\n      Court: <span class='lightText'>");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers.unbound.call(depth0, "court_name", {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("</span><br>\n      Attorney: <span class='lightText'>");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers.unbound.call(depth0, "attorney_name", {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("</span><br>\n      Phone no: <span class='lightText'>");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers.unbound.call(depth0, "telephone_number", {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("</span><br>\n      Comment: <span class='lightText'>");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers.unbound.call(depth0, "comments", {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("</span><br>\n    </div>\n    <div class='row whiteBack'>\n      <div class='col-xs-1'></div>\n      <div class='col-xs-10 dottedBottom'></div>\n    </div>\n  </div>\n");
  return buffer;
  }

  data.buffer.push("<div class='row verticalPad greyBottomBorder whiteBack'>\n  <div class=\"col-xs-1\">\n  </div>\n  <div class='col-xs-10 inTheMiddle'>\n    Administration orders<br>\n    against your name\n  </div>\n</div>\n");
  hashTypes = {};
  hashContexts = {};
  stack1 = helpers.each.call(depth0, "admin_orders", {hash:{},inverse:self.noop,fn:self.program(1, program1, data),contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data});
  if(stack1 || stack1 === 0) { data.buffer.push(stack1); }
  data.buffer.push("\n\n\n<!-- Modal -->\n<div class=\"modal fade\" id=\"myModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">\n  <div class=\"modal-dialog\">\n    <div class=\"modal-content\">\n      <div class=\"modal-header\">\n        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>\n        <h4 class=\"modal-title\" id=\"myModalLabel\">Admin Orders</h4>\n      </div>\n      <div class=\"modal-body\">\n        <b>Administration order?</b><br><br>\n        This is a court order granted at the request of you or a credit provider. Total debts may not exceed R50 000. It looks at your financial position and appoints an administrator. The administrator will negotiate payments to your creditors. You will make regular payments to your administrators that will divide the payments amongst your creditors.<br/><br/>\n        An administration order remains on your credit record for 5 – 10 years from the date of rehabilitation, but can be removed from the credit bureau sooner. You will need to settle the debts in full and appoint an attorney to assist you with removing the administration order from you credit record.\n      </div>\n    </div>\n  </div>\n</div>");
  return buffer;
  
}); });

define('appkit/templates/report/adverse', [], function(){ return Ember.Handlebars.template(function anonymous(Handlebars,depth0,helpers,partials,data) {
this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Ember.Handlebars.helpers); data = data || {};
  var buffer = '', stack1, hashTypes, hashContexts, escapeExpression=this.escapeExpression, self=this;

function program1(depth0,data) {
  
  var buffer = '', hashTypes, hashContexts;
  data.buffer.push("\n  <div class=\"row whiteBack verticalPad greyBottomBorder\">\n    <div class=\"col-xs-1\"></div>\n    <div class=\"col-xs-11 bottomPad\">\n      Subscriber: <span class='lightText'>");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers._triageMustache.call(depth0, "company", {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("</span><br>\n      Account no: <span class='lightText'>");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers._triageMustache.call(depth0, "account_number", {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("</span><br>\n      Action Date: <span class='lightText'>");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers._triageMustache.call(depth0, "date", {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("</span><br>\n      Amount: <span class='lightText'>");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers._triageMustache.call(depth0, "amount", {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("</span><br>\n      Status: <span class='lightText'>");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers._triageMustache.call(depth0, "status", {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("</span><br>\n      Comment: <span class='lightText'>");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers._triageMustache.call(depth0, "comments", {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("</span><br>\n    </div>\n    <div class='row whiteBack'>\n      <div class='col-xs-1'></div>\n      <div class='col-xs-10 dottedBottom'></div>\n    </div>\n  </div>\n");
  return buffer;
  }

  data.buffer.push("<div class='row bottomPad greyBottomBorder whiteBack'>\n  <div class=\"col-xs-1\">\n  </div>\n  <div class='col-xs-10 inTheMiddle'>\n    Your Adverse / Default <br>\n    Negative Information\n  </div>\n</div>\n");
  hashTypes = {};
  hashContexts = {};
  stack1 = helpers.each.call(depth0, "adverse_defaults", {hash:{},inverse:self.noop,fn:self.program(1, program1, data),contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data});
  if(stack1 || stack1 === 0) { data.buffer.push(stack1); }
  data.buffer.push("\n\n<!-- Modal -->\n<div class=\"modal fade\" id=\"myModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">\n  <div class=\"modal-dialog\">\n    <div class=\"modal-content\">\n      <div class=\"modal-header\">\n        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>\n        <h4 class=\"modal-title\" id=\"myModalLabel\">Adverse / Defaults</h4>\n      </div>\n      <div class=\"modal-body\">\n        <b>Missed payment, debit order returned or a ‘bounced’ cheque?</b><br><br>\n        This information will reflect negatively on your credit report. Bring your payments up to date and notify your bill issuer/credit provider to update your payment status.<br><br>\n\n        If you want to dispute any of the information in your credit report contact XDS (0860 937 000).\n      </div>\n    </div>\n  </div>\n</div>");
  return buffer;
  
}); });

define('appkit/templates/report/debt_review', [], function(){ return Ember.Handlebars.template(function anonymous(Handlebars,depth0,helpers,partials,data) {
this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Ember.Handlebars.helpers); data = data || {};
  var buffer = '', hashTypes, hashContexts, escapeExpression=this.escapeExpression;


  data.buffer.push("<div class='row verticalPad greyBottomBorder whiteBack'>\n  <div class=\"col-xs-1\">\n  </div>\n  <div class='col-xs-10 inTheMiddle'>\n    Your debt review status\n  </div>\n</div>\n<div class=\"row whiteBack verticalPad\">\n  <div class=\"col-xs-1\"></div>\n  <div class=\"col-xs-11 bottomPad\">\n    Debt Review Date: <span class='lightText'>");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers.unbound.call(depth0, "debt_review.date", {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("</span><br>\n    Debt Counsellor Name: <span class='lightText'>");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers.unbound.call(depth0, "debt_review.counsellor_name", {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("</span><br>\n    Debt Counsellor Contact No: <span class='lightText'>");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers.unbound.call(depth0, "debt_review.counsellor_contact_number", {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("</span><br>\n    Debt Review Status: <span class='lightText'>");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers.unbound.call(depth0, "debt_review.status", {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("</span><br>\n  </div>\n  <div class='row whiteBack'>\n    <div class='col-xs-1'></div>\n    <div class='col-xs-10 dottedBottom'></div>\n  </div>\n</div>\n\n<!-- Modal -->\n<div class=\"modal fade\" id=\"myModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">\n  <div class=\"modal-dialog\">\n    <div class=\"modal-content\">\n      <div class=\"modal-header\">\n        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>\n        <h4 class=\"modal-title\" id=\"myModalLabel\">Debt Review</h4>\n      </div>\n      <div class=\"modal-body\">\n        <b>Debt Review?</b><br><br>\n        If you’re under debt counselling/debt review, the information will reflect negatively on your credit report. You need to finalise your debt repayments to the debt counselling company.<br/><br/>\n        If you have terminated your agreement with the debt counselling company and want to remove the status:\n        <ol>\n          <li>Contact the debt counselling company to obtain a termination letter and forward such letter to the credit bureaus.</li>\n          <li>Note that if a court order is attached to the debt review status, you need to appoint an attorney to assist you.</li>\n        </ol>\n        If you want to dispute any of the information in your credit report contact XDS (0860 937 000).\n      </div>\n    </div>\n  </div>\n</div>");
  return buffer;
  
}); });

define('appkit/templates/report/default_listings', [], function(){ return Ember.Handlebars.template(function anonymous(Handlebars,depth0,helpers,partials,data) {
this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Ember.Handlebars.helpers); data = data || {};
  var buffer = '', stack1, hashTypes, hashContexts, escapeExpression=this.escapeExpression, self=this;

function program1(depth0,data) {
  
  var buffer = '', hashTypes, hashContexts;
  data.buffer.push("\n  <div class=\"row whiteBack verticalPad\">\n    <div class=\"col-xs-1\"></div>\n    <div class=\"col-xs-11 bottomPad\">\n      Company: <span class='lightText'>");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers.unbound.call(depth0, "company", {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("</span><br>\n      Account no: <span class='lightText'>");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers.unbound.call(depth0, "account_number", {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("</span><br>\n      Effective Date: <span class='lightText'>");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers.unbound.call(depth0, "date", {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("</span><br>\n      Amount: <span class='lightText'>");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers.unbound.call(depth0, "amount", {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("</span><br>\n      Status: <span class='lightText'>");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers.unbound.call(depth0, "status", {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("</span><br>\n      Comment: <span class='lightText'>");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers.unbound.call(depth0, "comments", {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("</span><br>\n    </div>\n    <div class='row whiteBack'>\n      <div class='col-xs-1'></div>\n      <div class='col-xs-10 dottedBottom'></div>\n    </div>\n  </div>\n");
  return buffer;
  }

  data.buffer.push("<div class='row bottomPad greyBottomBorder whiteBack'>\n  <div class=\"col-xs-1\">\n  </div>\n  <div class='col-xs-10 inTheMiddle'>\n    Your Default Listings\n  </div>\n</div>\n");
  hashTypes = {};
  hashContexts = {};
  stack1 = helpers.each.call(depth0, "default_listings", {hash:{},inverse:self.noop,fn:self.program(1, program1, data),contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data});
  if(stack1 || stack1 === 0) { data.buffer.push(stack1); }
  data.buffer.push("\n\n<!-- Modal -->\n<div class=\"modal fade\" id=\"myModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">\n  <div class=\"modal-dialog\">\n    <div class=\"modal-content\">\n      <div class=\"modal-header\">\n        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>\n        <h4 class=\"modal-title\" id=\"myModalLabel\">Default Listings</h4>\n      </div>\n      <div class=\"modal-body\">\n        <b>Defaults (missed a payment for more than 150 days):</b>\n        The debt might be handed over for collection. If you have any defaults, contact the creditor immediately to negotiate a repayment arrangement. If your default is paid up, forward the paid-up letter to the credit bureaus and advise them to update your account status as paid up on your credit report.<br><br>\n\n        If you want to dispute any of the information in your credit report contact XDS (0860 937 000).\n      </div>\n    </div>\n  </div>\n</div>\n");
  return buffer;
  
}); });

define('appkit/templates/report/enquiries', [], function(){ return Ember.Handlebars.template(function anonymous(Handlebars,depth0,helpers,partials,data) {
this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Ember.Handlebars.helpers); data = data || {};
  var buffer = '', stack1, hashTypes, hashContexts, escapeExpression=this.escapeExpression, self=this;

function program1(depth0,data) {
  
  var buffer = '', hashTypes, hashContexts;
  data.buffer.push("\n\n  <div class=\"row whiteBack topPad bottomPad\">\n    <div class=\"col-xs-12 bottomPad\">\n      Enquiry Date:<br>\n      &nbsp;<span class='lightText'>");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers.unbound.call(depth0, "date", {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("</span><br>\n      Requested By:<br>\n      &nbsp;<span class='lightText'>");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers.unbound.call(depth0, "credit_provider", {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("</span><br>\n      Type/Category Of Credit Provider:<br>\n      &nbsp;<span class='lightText'>");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers.unbound.call(depth0, "credit_provder_type", {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("</span><br>\n      Enquiry Reason:<br>\n      &nbsp;<span class='lightText'>");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers.unbound.call(depth0, "reason", {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("</span><br>\n    </div>\n    <div class='row whiteBack'>\n      <div class='col-xs-1'></div>\n      <div class='col-xs-10 dottedBottom'></div>\n    </div>\n  </div>\n");
  return buffer;
  }

  data.buffer.push("<div class='row bottomPad greyBottomBorder topPad'>\n  <div class=\"col-xs-1\">\n  </div>\n  <div class='col-xs-10 inTheMiddle'>\n    See here who's been looking at your credit report\n  </div>\n</div>\n");
  hashTypes = {};
  hashContexts = {};
  stack1 = helpers.each.call(depth0, "content", {hash:{},inverse:self.noop,fn:self.program(1, program1, data),contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data});
  if(stack1 || stack1 === 0) { data.buffer.push(stack1); }
  data.buffer.push("\n\n<!-- Modal -->\n<div class=\"modal fade\" id=\"myModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">\n  <div class=\"modal-dialog\">\n    <div class=\"modal-content\">\n      <div class=\"modal-header\">\n        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>\n        <h4 class=\"modal-title\" id=\"myModalLabel\">Enquiries</h4>\n      </div>\n      <div class=\"modal-body\">\n        See who’s been looking into your credit report. Some of the people that may be checking up on you from time to time include: financial institutions, insurance companies, credit providers and potential employers.<br/><br/>\n        If you are not aware of any of these enquiries please lodge a dispute with XDS (0860 937 000).<br/><br/>\n        Tip: Every time you apply for credit an enquiry is made into your credit report. Having too many enquiries in a short space of time can negatively affect your overall credit score. Therefore make sure you keep your credit applications to a minimum.\n      </div>\n    </div>\n  </div>\n</div>");
  return buffer;
  
}); });

define('appkit/templates/report/fraudulent', [], function(){ return Ember.Handlebars.template(function anonymous(Handlebars,depth0,helpers,partials,data) {
this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Ember.Handlebars.helpers); data = data || {};
  var buffer = '', stack1, hashTypes, hashContexts, options, helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;


  data.buffer.push("<div class='row whiteBack bottomPad topPad'>\n  <div class='col-xs-12 inTheMiddle'>\n    Fraudulent activities<br>\n    against your name.\n  </div>\n</div>\n\n<div class=\"whiteBack bottomPad row\">\n  <div class=\"col-xs-12\">\nID No. Verified At Home Affairs:<br>\n&nbsp;&nbsp;<span class='lightText'>");
  hashTypes = {};
  hashContexts = {};
  options = {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data};
  data.buffer.push(escapeExpression(((stack1 = helpers.xxxIfNoValue || depth0.xxxIfNoValue),stack1 ? stack1.call(depth0, "fraud_indicators.home_affairs_verification", options) : helperMissing.call(depth0, "xxxIfNoValue", "fraud_indicators.home_affairs_verification", options))));
  data.buffer.push("</span>                           <br>\nID No. Deceased At Home Affairs:    <br>\n    &nbsp;&nbsp;<span class='lightText'>");
  hashTypes = {};
  hashContexts = {};
  options = {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data};
  data.buffer.push(escapeExpression(((stack1 = helpers.xxxIfNoValue || depth0.xxxIfNoValue),stack1 ? stack1.call(depth0, "fraud_indicators.deceased_status", options) : helperMissing.call(depth0, "xxxIfNoValue", "fraud_indicators.deceased_status", options))));
  data.buffer.push("</span>                                   <br>\nID No. Found On Fraud Database:             <br>\n    &nbsp;&nbsp;<span class='lightText'>");
  hashTypes = {};
  hashContexts = {};
  options = {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data};
  data.buffer.push(escapeExpression(((stack1 = helpers.xxxIfNoValue || depth0.xxxIfNoValue),stack1 ? stack1.call(depth0, "fraud_indicators.safps_listing", options) : helperMissing.call(depth0, "xxxIfNoValue", "fraud_indicators.safps_listing", options))));
  data.buffer.push("</span>                                           <br>\nID No. Found on Employer Fraud Database             <br>\n    &nbsp;&nbsp;<span class='lightText'>");
  hashTypes = {};
  hashContexts = {};
  options = {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data};
  data.buffer.push(escapeExpression(((stack1 = helpers.xxxIfNoValue || depth0.xxxIfNoValue),stack1 ? stack1.call(depth0, "fraud_indicators.employer_fraud_verification", options) : helperMissing.call(depth0, "xxxIfNoValue", "fraud_indicators.employer_fraud_verification", options))));
  data.buffer.push("</span>                                                  <br>\nID No. Found On Protective Register                          <br>\n    &nbsp;&nbsp;<span class='lightText'>");
  hashTypes = {};
  hashContexts = {};
  options = {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data};
  data.buffer.push(escapeExpression(((stack1 = helpers.xxxIfNoValue || depth0.xxxIfNoValue),stack1 ? stack1.call(depth0, "fraud_indicators.protective_verification", options) : helperMissing.call(depth0, "xxxIfNoValue", "fraud_indicators.protective_verification", options))));
  data.buffer.push("</span>                                                          <br>\n  </div>\n</div>\n\n<!-- Modal -->\n<div class=\"modal fade\" id=\"myModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">\n  <div class=\"modal-dialog\">\n    <div class=\"modal-content\">\n      <div class=\"modal-header\">\n        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>\n        <h4 class=\"modal-title\" id=\"myModalLabel\">Fraudulent activities</h4>\n      </div>\n      <div class=\"modal-body\">\n        This section displays information verified by Home Affairs.\n        Finding your details here means that you’ve been listed on the records of the South African Fraud Prevention Services (SAFPS) for potential fraudulent activities. This will restrict you from accessing credit.<br><br>\n\n        <b>For any fraud queries:</b>\n\n        <ul>\n          <li>Contact the SAFPS (086 010 1248) to find out who posted the fraud notice.</li>\n          <li>Then contact this company to get the full details of the fraud notice.</li>\n          <li>The services of an attorney should be procured to make this process less painful.</li>\n        </ul>\n\n      </div>\n    </div>\n  </div>\n</div>");
  return buffer;
  
}); });

define('appkit/templates/report/judgements', [], function(){ return Ember.Handlebars.template(function anonymous(Handlebars,depth0,helpers,partials,data) {
this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Ember.Handlebars.helpers); data = data || {};
  var buffer = '', stack1, hashTypes, hashContexts, escapeExpression=this.escapeExpression, self=this;

function program1(depth0,data) {
  
  var buffer = '', hashTypes, hashContexts;
  data.buffer.push("\n  <div class=\"row whiteBack verticalPad\">\n    <div class=\"col-xs-1\"></div>\n    <div class=\"col-xs-11 bottomPad\">\n      Case no: <span class='lightText'>");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers.unbound.call(depth0, "case_number", {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("</span><br>\n      Filing Date: <span class='lightText'>");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers.unbound.call(depth0, "filing_date", {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("</span><br>\n      Judgement Type: <span class='lightText'>");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers.unbound.call(depth0, "case_type", {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("</span><br>\n      Amount: <span class='lightText'>");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers.unbound.call(depth0, "amount", {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("</span><br>\n      Plaintiff: <span class='lightText'>");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers.unbound.call(depth0, "plaintiff_name", {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("</span><br>\n      Court: <span class='lightText'>");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers.unbound.call(depth0, "court_name", {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("</span><br>\n      Attorney: <span class='lightText'>");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers.unbound.call(depth0, "attorney_name", {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("</span><br>\n      Phone no: <span class='lightText'>");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers.unbound.call(depth0, "telephone_number", {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("</span><br>\n      Comment: <span class='lightText'>");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers.unbound.call(depth0, "comments", {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("</span><br>\n    </div>\n    <div class='row whiteBack'>\n      <div class='col-xs-1'></div>\n      <div class='col-xs-10 dottedBottom'></div>\n    </div>\n  </div>\n");
  return buffer;
  }

  data.buffer.push("<div class='row verticalPad greyBottomBorder whiteBack'>\n  <div class=\"col-xs-1\">\n  </div>\n  <div class='col-xs-10 inTheMiddle'>\n    Judgements against you\n  </div>\n</div>\n");
  hashTypes = {};
  hashContexts = {};
  stack1 = helpers.each.call(depth0, "judgements", {hash:{},inverse:self.noop,fn:self.program(1, program1, data),contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data});
  if(stack1 || stack1 === 0) { data.buffer.push(stack1); }
  data.buffer.push("\n\n<!-- Modal -->\n<div class=\"modal fade\" id=\"myModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">\n  <div class=\"modal-dialog\">\n    <div class=\"modal-content\">\n      <div class=\"modal-header\">\n        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>\n        <h4 class=\"modal-title\" id=\"myModalLabel\">Judgements</h4>\n      </div>\n      <div class=\"modal-body\">\n        <b>Judgements?</b><br><br>\n\n        A credit provider can take out a judgement against you if you have not paid your account and have not responded to reminder letters to pay. A judgement is granted when a court gives you the instruction to pay the outstanding account. A judgement remains on your credit report for 5 years.<br><br>\n\n        <b>To remove judgements:</b>\n        <ol>\n          <li>Pay the debt in full</li>\n          <li>Get written consent from the credit provider</li>\n          <li>Appoint an attorney to assist you with removal of the judgement</li>\n        </ol>\n\n        If you want to dispute any of the information in your credit report contact XDS (0860 937 000)      </div>\n    </div>\n  </div>\n</div>");
  return buffer;
  
}); });

define('appkit/templates/report/negative', [], function(){ return Ember.Handlebars.template(function anonymous(Handlebars,depth0,helpers,partials,data) {
this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Ember.Handlebars.helpers); data = data || {};
  var buffer = '', stack1, stack2, hashContexts, hashTypes, options, helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression, self=this;

function program1(depth0,data) {
  
  var buffer = '', stack1, hashContexts, hashTypes, options;
  data.buffer.push("\n<div class='row whiteBack verticalPad'>\n  <div class='col-xs-1'></div>\n  <div ");
  hashContexts = {'class': depth0};
  hashTypes = {'class': "STRING"};
  options = {hash:{
    'class': ("adverseClass :col-xs-10 :whiteText :verticalPad")
  },contexts:[],types:[],hashContexts:hashContexts,hashTypes:hashTypes,data:data};
  data.buffer.push(escapeExpression(((stack1 = helpers['bind-attr'] || depth0['bind-attr']),stack1 ? stack1.call(depth0, options) : helperMissing.call(depth0, "bind-attr", options))));
  data.buffer.push(">\n    <div clas=\"row\">\n      <div class=\"col-xs-1\">\n      </div>\n      <div class=\"col-xs-9\">\n        Adverse / Defaults\n      </div>\n      <div class=\"col-xs-2 greyLeftBorder inTheMiddle\">\n        ");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers._triageMustache.call(depth0, "adverse_defaults.length", {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("\n      </div>\n    </div>\n  </div>\n</div>\n");
  return buffer;
  }

function program3(depth0,data) {
  
  var buffer = '', stack1, hashContexts, hashTypes, options;
  data.buffer.push("\n<div class='row whiteBack verticalPad'>\n  <div class='col-xs-1'></div>\n  <div ");
  hashContexts = {'class': depth0};
  hashTypes = {'class': "STRING"};
  options = {hash:{
    'class': ("defaultListingsClass :col-xs-10 :whiteText :verticalPad")
  },contexts:[],types:[],hashContexts:hashContexts,hashTypes:hashTypes,data:data};
  data.buffer.push(escapeExpression(((stack1 = helpers['bind-attr'] || depth0['bind-attr']),stack1 ? stack1.call(depth0, options) : helperMissing.call(depth0, "bind-attr", options))));
  data.buffer.push(">\n    <div clas=\"row\">\n      <div class=\"col-xs-1\">\n      </div>\n      <div class=\"col-xs-9\">\n        Default Listings\n      </div>\n      <div class=\"col-xs-2 greyLeftBorder inTheMiddle\">\n        ");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers._triageMustache.call(depth0, "default_listings.length", {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("\n      </div>\n    </div>\n  </div>\n</div>\n");
  return buffer;
  }

function program5(depth0,data) {
  
  var buffer = '', stack1, hashContexts, hashTypes, options;
  data.buffer.push("\n<div class='row whiteBack verticalPad'>\n  <div class='col-xs-1'></div>\n  <div ");
  hashContexts = {'class': depth0};
  hashTypes = {'class': "STRING"};
  options = {hash:{
    'class': ("judgementsClass :col-xs-10 :whiteText :verticalPad")
  },contexts:[],types:[],hashContexts:hashContexts,hashTypes:hashTypes,data:data};
  data.buffer.push(escapeExpression(((stack1 = helpers['bind-attr'] || depth0['bind-attr']),stack1 ? stack1.call(depth0, options) : helperMissing.call(depth0, "bind-attr", options))));
  data.buffer.push(">\n    <div clas=\"row\">\n      <div class=\"col-xs-1\">\n      </div>\n      <div class=\"col-xs-9\">\n        Judgements\n      </div>\n      <div class=\"col-xs-2 greyLeftBorder inTheMiddle\">\n        ");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers._triageMustache.call(depth0, "judgements.length", {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("\n      </div>\n    </div>\n  </div>\n</div>\n");
  return buffer;
  }

function program7(depth0,data) {
  
  var buffer = '', stack1, hashContexts, hashTypes, options;
  data.buffer.push("\n<div class='row whiteBack verticalPad'>\n  <div class='col-xs-1'></div>\n  <div ");
  hashContexts = {'class': depth0};
  hashTypes = {'class': "STRING"};
  options = {hash:{
    'class': ("adminOrdersClass :col-xs-10 :whiteText :verticalPad")
  },contexts:[],types:[],hashContexts:hashContexts,hashTypes:hashTypes,data:data};
  data.buffer.push(escapeExpression(((stack1 = helpers['bind-attr'] || depth0['bind-attr']),stack1 ? stack1.call(depth0, options) : helperMissing.call(depth0, "bind-attr", options))));
  data.buffer.push(">\n    <div clas=\"row\">\n      <div class=\"col-xs-1\">\n      </div>\n      <div class=\"col-xs-9\">\n        Admin Orders\n      </div>\n      <div class=\"col-xs-2 greyLeftBorder inTheMiddle\">\n        ");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers._triageMustache.call(depth0, "admin_orders.length", {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("\n      </div>\n    </div>\n  </div>\n</div>\n");
  return buffer;
  }

function program9(depth0,data) {
  
  var buffer = '', stack1, hashContexts, hashTypes, options;
  data.buffer.push("\n<div class='row whiteBack verticalPad'>\n  <div class='col-xs-1'></div>\n  <div ");
  hashContexts = {'class': depth0};
  hashTypes = {'class': "STRING"};
  options = {hash:{
    'class': ("sequestrationsClass :col-xs-10 :whiteText :verticalPad")
  },contexts:[],types:[],hashContexts:hashContexts,hashTypes:hashTypes,data:data};
  data.buffer.push(escapeExpression(((stack1 = helpers['bind-attr'] || depth0['bind-attr']),stack1 ? stack1.call(depth0, options) : helperMissing.call(depth0, "bind-attr", options))));
  data.buffer.push(">\n    <div clas=\"row\">\n      <div class=\"col-xs-1\">\n      </div>\n      <div class=\"col-xs-9\">\n        Sequestrations\n      </div>\n      <div class=\"col-xs-2 greyLeftBorder inTheMiddle\">\n        ");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers._triageMustache.call(depth0, "sequestrations.length", {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("\n      </div>\n    </div>\n  </div>\n</div>\n");
  return buffer;
  }

function program11(depth0,data) {
  
  var buffer = '', stack1, hashContexts, hashTypes, options;
  data.buffer.push("\n<div class='row whiteBack verticalPad'>\n  <div class='col-xs-1'></div>\n  <div ");
  hashContexts = {'class': depth0};
  hashTypes = {'class': "STRING"};
  options = {hash:{
    'class': ("rehabilitationOrdersClass :col-xs-10 :whiteText :verticalPad")
  },contexts:[],types:[],hashContexts:hashContexts,hashTypes:hashTypes,data:data};
  data.buffer.push(escapeExpression(((stack1 = helpers['bind-attr'] || depth0['bind-attr']),stack1 ? stack1.call(depth0, options) : helperMissing.call(depth0, "bind-attr", options))));
  data.buffer.push(">\n    <div clas=\"row\">\n      <div class=\"col-xs-1\">\n      </div>\n      <div class=\"col-xs-9\">\n        Rehabilitation Orders\n      </div>\n      <div class=\"col-xs-2 greyLeftBorder inTheMiddle\">\n        ");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers._triageMustache.call(depth0, "rehabilitation_orders.length", {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("\n      </div>\n    </div>\n  </div>\n</div>\n");
  return buffer;
  }

function program13(depth0,data) {
  
  var buffer = '', stack1, stack2, hashTypes, hashContexts, options;
  data.buffer.push("\n");
  hashTypes = {};
  hashContexts = {};
  options = {hash:{},inverse:self.noop,fn:self.program(14, program14, data),contexts:[depth0],types:["STRING"],hashContexts:hashContexts,hashTypes:hashTypes,data:data};
  stack2 = ((stack1 = helpers['link-to'] || depth0['link-to']),stack1 ? stack1.call(depth0, "report.debt_review", options) : helperMissing.call(depth0, "link-to", "report.debt_review", options));
  if(stack2 || stack2 === 0) { data.buffer.push(stack2); }
  data.buffer.push("\n");
  return buffer;
  }
function program14(depth0,data) {
  
  var buffer = '', stack1, hashContexts, hashTypes, options;
  data.buffer.push("\n<div class='row whiteBack verticalPad'>\n  <div class='col-xs-1'></div>\n  <div ");
  hashContexts = {'class': depth0};
  hashTypes = {'class': "STRING"};
  options = {hash:{
    'class': ("debtReviewStatusClass :col-xs-10 :whiteText :verticalPad")
  },contexts:[],types:[],hashContexts:hashContexts,hashTypes:hashTypes,data:data};
  data.buffer.push(escapeExpression(((stack1 = helpers['bind-attr'] || depth0['bind-attr']),stack1 ? stack1.call(depth0, options) : helperMissing.call(depth0, "bind-attr", options))));
  data.buffer.push(">\n    <div clas=\"row\">\n      <div class=\"col-xs-1\">\n      </div>\n      <div class=\"col-xs-9\">\n        You are under debt review\n      </div>\n    </div>\n  </div>\n</div>\n");
  return buffer;
  }

  data.buffer.push("<div class='row whiteBack bottomPad topPad'>\n  <div class='col-xs-12 inTheMiddle'>\n    Check any negative information<br>\n    against you name\n  </div>\n</div>\n\n");
  hashContexts = {'disabledWhen': depth0};
  hashTypes = {'disabledWhen': "ID"};
  options = {hash:{
    'disabledWhen': ("disabledAdverseLink")
  },inverse:self.noop,fn:self.program(1, program1, data),contexts:[depth0],types:["STRING"],hashContexts:hashContexts,hashTypes:hashTypes,data:data};
  stack2 = ((stack1 = helpers['link-to'] || depth0['link-to']),stack1 ? stack1.call(depth0, "report.adverse", options) : helperMissing.call(depth0, "link-to", "report.adverse", options));
  if(stack2 || stack2 === 0) { data.buffer.push(stack2); }
  data.buffer.push("\n\n");
  hashContexts = {'disabledWhen': depth0};
  hashTypes = {'disabledWhen': "ID"};
  options = {hash:{
    'disabledWhen': ("disabledDefaultListingsLink")
  },inverse:self.noop,fn:self.program(3, program3, data),contexts:[depth0],types:["STRING"],hashContexts:hashContexts,hashTypes:hashTypes,data:data};
  stack2 = ((stack1 = helpers['link-to'] || depth0['link-to']),stack1 ? stack1.call(depth0, "report.default_listings", options) : helperMissing.call(depth0, "link-to", "report.default_listings", options));
  if(stack2 || stack2 === 0) { data.buffer.push(stack2); }
  data.buffer.push("\n\n");
  hashContexts = {'disabledWhen': depth0};
  hashTypes = {'disabledWhen': "ID"};
  options = {hash:{
    'disabledWhen': ("disabledJudgementsLink")
  },inverse:self.noop,fn:self.program(5, program5, data),contexts:[depth0],types:["STRING"],hashContexts:hashContexts,hashTypes:hashTypes,data:data};
  stack2 = ((stack1 = helpers['link-to'] || depth0['link-to']),stack1 ? stack1.call(depth0, "report.judgements", options) : helperMissing.call(depth0, "link-to", "report.judgements", options));
  if(stack2 || stack2 === 0) { data.buffer.push(stack2); }
  data.buffer.push("\n\n<div class='row whiteBack verticalPad'>\n  <div class='col-xs-12 inTheMiddle'>\n    Court notices\n  </div>\n</div>\n\n");
  hashContexts = {'disabledWhen': depth0};
  hashTypes = {'disabledWhen': "ID"};
  options = {hash:{
    'disabledWhen': ("disabledAdminOrdersLink")
  },inverse:self.noop,fn:self.program(7, program7, data),contexts:[depth0],types:["STRING"],hashContexts:hashContexts,hashTypes:hashTypes,data:data};
  stack2 = ((stack1 = helpers['link-to'] || depth0['link-to']),stack1 ? stack1.call(depth0, "report.admin_orders", options) : helperMissing.call(depth0, "link-to", "report.admin_orders", options));
  if(stack2 || stack2 === 0) { data.buffer.push(stack2); }
  data.buffer.push("\n\n");
  hashContexts = {'disabledWhen': depth0};
  hashTypes = {'disabledWhen': "ID"};
  options = {hash:{
    'disabledWhen': ("disabledSequestrationsLink")
  },inverse:self.noop,fn:self.program(9, program9, data),contexts:[depth0],types:["STRING"],hashContexts:hashContexts,hashTypes:hashTypes,data:data};
  stack2 = ((stack1 = helpers['link-to'] || depth0['link-to']),stack1 ? stack1.call(depth0, "report.sequestrations", options) : helperMissing.call(depth0, "link-to", "report.sequestrations", options));
  if(stack2 || stack2 === 0) { data.buffer.push(stack2); }
  data.buffer.push("\n\n");
  hashContexts = {'disabledWhen': depth0};
  hashTypes = {'disabledWhen': "ID"};
  options = {hash:{
    'disabledWhen': ("disabledRehabLink")
  },inverse:self.noop,fn:self.program(11, program11, data),contexts:[depth0],types:["STRING"],hashContexts:hashContexts,hashTypes:hashTypes,data:data};
  stack2 = ((stack1 = helpers['link-to'] || depth0['link-to']),stack1 ? stack1.call(depth0, "report.rehab_orders", options) : helperMissing.call(depth0, "link-to", "report.rehab_orders", options));
  if(stack2 || stack2 === 0) { data.buffer.push(stack2); }
  data.buffer.push("\n\n");
  hashTypes = {};
  hashContexts = {};
  stack2 = helpers['if'].call(depth0, "showDebtReviewStatus", {hash:{},inverse:self.noop,fn:self.program(13, program13, data),contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data});
  if(stack2 || stack2 === 0) { data.buffer.push(stack2); }
  data.buffer.push("\n\n<!-- Modal -->\n<div class=\"modal fade\" id=\"myModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">\n  <div class=\"modal-dialog\">\n    <div class=\"modal-content\">\n      <div class=\"modal-header\">\n        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>\n        <h4 class=\"modal-title\" id=\"myModalLabel\">Negative Information</h4>\n      </div>\n      <div class=\"modal-body\">\n        This section shows any negative information on your record and can include things like defaults, judgements and late payments.\n        If you have any negative information it’s crucial that you address the situation as soon as possible.<br><br>\n\n        If you want to dispute any of the information in your credit report contact XDS (0860 937 000).<br><br>\n\n        <b>Tip:</b> Once all your negative information has been addressed, it may take up to 6 months of \"clean living\" to rectify your credit profile.\n      </div>\n    </div>\n  </div>\n</div>");
  return buffer;
  
}); });

define('appkit/templates/report/profile', [], function(){ return Ember.Handlebars.template(function anonymous(Handlebars,depth0,helpers,partials,data) {
this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Ember.Handlebars.helpers); data = data || {};
  var buffer = '', hashTypes, hashContexts, escapeExpression=this.escapeExpression;


  data.buffer.push("<div class='row whiteBack bottomPad topPad'>\n  <div class='col-xs-12 inTheMiddle'>\n    This is your personal info<br>\n    Make sure your info is up to date\n  </div>\n</div>\n\n\n<div class='row whiteBack'>\n  <div class='col-xs-1'></div>\n  <div class='col-xs-5'>\n    Id No:\n  </div>\n  <div class='col-xs-6 noLeftPadding'>\n    ");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers.unbound.call(depth0, "not-blank", "personal_details.id_number", {hash:{},contexts:[depth0,depth0],types:["ID","ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("\n  </div>\n  <div class='col-xs-1'></div>\n  <div class='col-xs-10 dottedBottom'></div>\n</div>\n<div class='row whiteBack'>\n  <div class='col-xs-1'></div>\n  <div class='col-xs-5'>\n    Surname:\n  </div>\n  <div class='col-xs-6 noLeftPadding'>\n    ");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers.unbound.call(depth0, "not-blank", "personal_details.surname", {hash:{},contexts:[depth0,depth0],types:["ID","ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("\n  </div>\n  <div class='col-xs-1'></div>\n  <div class='col-xs-10 dottedBottom'></div>\n</div>\n<div class='row whiteBack'>\n  <div class='col-xs-1'></div>\n  <div class='col-xs-5'>\n    First name:\n  </div>\n  <div class='col-xs-6 noLeftPadding'>\n    ");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers.unbound.call(depth0, "not-blank", "personal_details.first_name", {hash:{},contexts:[depth0,depth0],types:["ID","ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("\n  </div>\n  <div class='col-xs-1'></div>\n  <div class='col-xs-10 dottedBottom'></div>\n</div>\n\n<div class='row whiteBack'>\n  <div class='col-xs-1'></div>\n  <div class='col-xs-5'>\n    Second name:\n  </div>\n  <div class='col-xs-6 noLeftPadding'>\n    ");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers.unbound.call(depth0, "not-blank", "personal_details.middle_name", {hash:{},contexts:[depth0,depth0],types:["ID","ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("\n  </div>\n  <div class='col-xs-1'></div>\n  <div class='col-xs-10 dottedBottom'></div>\n</div>\n<div class='row whiteBack'>\n  <div class='col-xs-1'></div>\n  <div class='col-xs-5'>\n    Title:\n  </div>\n  <div class='col-xs-6 noLeftPadding'>\n    ");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers.unbound.call(depth0, "not-blank", "personal_details.title", {hash:{},contexts:[depth0,depth0],types:["ID","ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("\n  </div>\n  <div class='col-xs-1'></div>\n  <div class='col-xs-10 dottedBottom'></div>\n</div>\n<div class='row whiteBack'>\n  <div class='col-xs-1'></div>\n  <div class='col-xs-5'>\n    Gender:\n  </div>\n  <div class='col-xs-6 noLeftPadding'>\n    ");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers.unbound.call(depth0, "not-blank", "personal_details.gender", {hash:{},contexts:[depth0,depth0],types:["ID","ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("\n  </div>\n  <div class='col-xs-1'></div>\n  <div class='col-xs-10 dottedBottom'></div>\n</div>\n<div class='row whiteBack'>\n  <div class='col-xs-1'></div>\n  <div class='col-xs-5'>\n    Date of birth:\n  </div>\n  <div class='col-xs-6 noLeftPadding'>\n    ");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers.unbound.call(depth0, "not-blank", "personal_details.date_of_birth", {hash:{},contexts:[depth0,depth0],types:["ID","ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("\n  </div>\n  <div class='col-xs-1'></div>\n  <div class='col-xs-10 dottedBottom'></div>\n</div>\n<div class='row whiteBack'>\n  <div class='col-xs-1'></div>\n  <div class='col-xs-5'>\n    Marital status:\n  </div>\n  <div class='col-xs-6 noLeftPadding'>\n    ");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers.unbound.call(depth0, "not-blank", "personal_details.marital_status", {hash:{},contexts:[depth0,depth0],types:["ID","ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("\n  </div>\n  <div class='col-xs-1'></div>\n  <div class='col-xs-10 dottedBottom'></div>\n</div>\n\n<div class='row whiteBack'>\n  <div class='col-xs-1'></div>\n  <div class='col-xs-5'>\n    Passport No:\n  </div>\n  <div class='col-xs-6 noLeftPadding'>\n    ");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers.unbound.call(depth0, "not-blank", "personal_details.passport_number", {hash:{},contexts:[depth0,depth0],types:["ID","ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("\n  </div>\n  <div class='col-xs-1'></div>\n  <div class='col-xs-10 dottedBottom'></div>\n</div>\n<div class='row whiteBack'>\n  <div class='col-xs-1'></div>\n  <div class='col-xs-5'>\n    Residential address:\n  </div>\n  <div class='col-xs-6 noLeftPadding'>\n    ");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers.unbound.call(depth0, "not-blank", "personal_details.residential_address", {hash:{},contexts:[depth0,depth0],types:["ID","ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("\n  </div>\n  <div class='col-xs-1'></div>\n  <div class='col-xs-10 dottedBottom'></div>\n</div>\n<div class='row whiteBack'>\n  <div class='col-xs-1'></div>\n  <div class='col-xs-5'>\n    Postal address:\n  </div>\n  <div class='col-xs-6 noLeftPadding'>\n    ");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers.unbound.call(depth0, "not-blank", "personal_details.postal_address", {hash:{},contexts:[depth0,depth0],types:["ID","ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("\n  </div>\n  <div class='col-xs-1'></div>\n  <div class='col-xs-10 dottedBottom'></div>\n</div>\n<div class='row whiteBack'>\n  <div class='col-xs-1'></div>\n  <div class='col-xs-5'>\n    Telephone No. (H)\n  </div>\n  <div class='col-xs-6 noLeftPadding'>\n    ");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers.unbound.call(depth0, "not-blank", "personal_details.home_phone", {hash:{},contexts:[depth0,depth0],types:["ID","ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("\n  </div>\n  <div class='col-xs-1'></div>\n  <div class='col-xs-10 dottedBottom'></div>\n</div>\n<div class='row whiteBack'>\n  <div class='col-xs-1'></div>\n  <div class='col-xs-5'>\n    Telephone No. (W)\n  </div>\n  <div class='col-xs-6 noLeftPadding'>\n    ");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers.unbound.call(depth0, "not-blank", "personal_details.work_phone", {hash:{},contexts:[depth0,depth0],types:["ID","ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("\n  </div>\n  <div class='col-xs-1'></div>\n  <div class='col-xs-10 dottedBottom'></div>\n</div>\n<div class='row whiteBack'>\n  <div class='col-xs-1'></div>\n  <div class='col-xs-5'>\n    Cellular / Mobile\n  </div>\n  <div class='col-xs-6 noLeftPadding'>\n    ");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers.unbound.call(depth0, "not-blank", "personal_details.mobile_phone", {hash:{},contexts:[depth0,depth0],types:["ID","ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("\n  </div>\n  <div class='col-xs-1'></div>\n  <div class='col-xs-10 dottedBottom'></div>\n</div>\n<div class='row whiteBack'>\n  <div class='col-xs-1'></div>\n  <div class='col-xs-5'>\n    Email address\n  </div>\n  <div class='col-xs-6 noLeftPadding'>\n    ");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers.unbound.call(depth0, "not-blank", "personal_details.email", {hash:{},contexts:[depth0,depth0],types:["ID","ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("\n  </div>\n  <div class='col-xs-1'></div>\n  <div class='col-xs-10 dottedBottom'></div>\n</div>\n<div class='row whiteBack'>\n  <div class='col-xs-1'></div>\n  <div class='col-xs-5'>\n    Current employer\n  </div>\n  <div class='col-xs-6 noLeftPadding'>\n    ");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers.unbound.call(depth0, "not-blank", "personal_details.current_employer", {hash:{},contexts:[depth0,depth0],types:["ID","ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("\n  </div>\n  <div class='col-xs-1'></div>\n  <div class='col-xs-10 dottedBottom'></div>\n</div>\n\n<!-- Modal -->\n<div class=\"modal fade\" id=\"myModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">\n  <div class=\"modal-dialog\">\n    <div class=\"modal-content\">\n      <div class=\"modal-header\">\n        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>\n        <h4 class=\"modal-title\" id=\"myModalLabel\">Personal Info</h4>\n      </div>\n      <div class=\"modal-body\">\n        Check your personal details. Make sure that XDS has your latest information. Contact XDS on 0860 937 000 to make changes, e.g. change address details.\n      </div>\n    </div>\n  </div>\n</div>\n");
  return buffer;
  
}); });

define('appkit/templates/report/rehab_orders', [], function(){ return Ember.Handlebars.template(function anonymous(Handlebars,depth0,helpers,partials,data) {
this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Ember.Handlebars.helpers); data = data || {};
  var buffer = '', stack1, hashTypes, hashContexts, escapeExpression=this.escapeExpression, self=this;

function program1(depth0,data) {
  
  var buffer = '', hashTypes, hashContexts;
  data.buffer.push("\n  <div class=\"row whiteBack verticalPad\">\n    <div class=\"col-xs-1\"></div>\n    <div class=\"col-xs-11 bottomPad\">\n      Case no: <span class='lightText'>");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers.unbound.call(depth0, "case_number", {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("</span><br>\n      Filing Date: <span class='lightText'>");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers.unbound.call(depth0, "filing_date", {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("</span><br>\n      Judgement Type: <span class='lightText'>");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers.unbound.call(depth0, "case_type", {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("</span><br>\n      Amount: <span class='lightText'>");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers.unbound.call(depth0, "amount", {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("</span><br>\n      Plaintiff: <span class='lightText'>");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers.unbound.call(depth0, "plaintiff_name", {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("</span><br>\n      Court: <span class='lightText'>");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers.unbound.call(depth0, "court_name", {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("</span><br>\n      Attorney: <span class='lightText'>");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers.unbound.call(depth0, "attorney_name", {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("</span><br>\n      Phone no: <span class='lightText'>");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers.unbound.call(depth0, "telephone_number", {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("</span><br>\n      Comment: <span class='lightText'>");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers.unbound.call(depth0, "comments", {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("</span><br>\n    </div>\n    <div class='row whiteBack'>\n      <div class='col-xs-1'></div>\n      <div class='col-xs-10 dottedBottom'></div>\n    </div>\n  </div>\n");
  return buffer;
  }

  data.buffer.push("<div class='row verticalPad greyBottomBorder whiteBack'>\n  <div class=\"col-xs-1\">\n  </div>\n  <div class='col-xs-10 inTheMiddle'>\n    Rehabilitation orders against your name\n  </div>\n</div>\n");
  hashTypes = {};
  hashContexts = {};
  stack1 = helpers.each.call(depth0, "rehabilitation_orders", {hash:{},inverse:self.noop,fn:self.program(1, program1, data),contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data});
  if(stack1 || stack1 === 0) { data.buffer.push(stack1); }
  data.buffer.push("\n\n<!-- Modal -->\n<div class=\"modal fade\" id=\"myModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">\n  <div class=\"modal-dialog\">\n    <div class=\"modal-content\">\n      <div class=\"modal-header\">\n        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>\n        <h4 class=\"modal-title\" id=\"myModalLabel\">Rehabilitation Orders</h4>\n      </div>\n      <div class=\"modal-body\">\n        Rehabilitation is the process to restore an insolvent (unable to pay debts owed) individual to the status of solvent (able to pay debts owed).<br/><br/>\n        Rehabilitation occurs automatically 10 years from the date of Sequestration. If you want to be rehabilitated earlier you must apply to the High Court for a Rehabilitation Order.<br/><br/>\n        Rehabilitation orders remains on the Credit Bureaus database for a period of 5 years from date of rehabilitation.<br/><br/>\n      </div>\n    </div>\n  </div>\n</div>");
  return buffer;
  
}); });

define('appkit/templates/report/sequestrations', [], function(){ return Ember.Handlebars.template(function anonymous(Handlebars,depth0,helpers,partials,data) {
this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Ember.Handlebars.helpers); data = data || {};
  var buffer = '', stack1, hashTypes, hashContexts, escapeExpression=this.escapeExpression, self=this;

function program1(depth0,data) {
  
  var buffer = '', hashTypes, hashContexts;
  data.buffer.push("\n  <div class=\"row whiteBack verticalPad\">\n    <div class=\"col-xs-1\"></div>\n    <div class=\"col-xs-11 bottomPad\">\n      Case no: <span class='lightText'>");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers.unbound.call(depth0, "case_number", {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("</span><br>\n      Issue Date: <span class='lightText'>");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers.unbound.call(depth0, "filing_date", {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("</span><br>\n      Judgement Type: <span class='lightText'>");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers.unbound.call(depth0, "case_type", {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("</span><br>\n      Amount: <span class='lightText'>");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers.unbound.call(depth0, "amount", {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("</span><br>\n      Plaintiff: <span class='lightText'>");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers.unbound.call(depth0, "plaintiff_name", {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("</span><br>\n      Court: <span class='lightText'>");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers.unbound.call(depth0, "court_name", {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("</span><br>\n      Attorney: <span class='lightText'>");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers.unbound.call(depth0, "attorney_name", {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("</span><br>\n      Phone no: <span class='lightText'>");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers.unbound.call(depth0, "telephone_number", {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("</span><br>\n      Comment: <span class='lightText'>");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers.unbound.call(depth0, "comments", {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("</span><br>\n    </div>\n    <div class='row whiteBack'>\n      <div class='col-xs-1'></div>\n      <div class='col-xs-10 dottedBottom'></div>\n    </div>\n  </div>\n");
  return buffer;
  }

  data.buffer.push("<div class='row verticalPad greyBottomBorder whiteBack'>\n  <div class=\"col-xs-1\">\n  </div>\n  <div class='col-xs-10 inTheMiddle'>\n    Sequestration and provisional sequestration orders against your name\n  </div>\n</div>\n");
  hashTypes = {};
  hashContexts = {};
  stack1 = helpers.each.call(depth0, "sequestrations", {hash:{},inverse:self.noop,fn:self.program(1, program1, data),contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data});
  if(stack1 || stack1 === 0) { data.buffer.push(stack1); }
  data.buffer.push("\n\n\n<!-- Modal -->\n<div class=\"modal fade\" id=\"myModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">\n  <div class=\"modal-dialog\">\n    <div class=\"modal-content\">\n      <div class=\"modal-header\">\n        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>\n        <h4 class=\"modal-title\" id=\"myModalLabel\">Sequestration</h4>\n      </div>\n      <div class=\"modal-body\">\n        <b>Sequestration?</b><br><br>\n        If you are insolvent (unable to pay debts owed), a creditor may apply to the court for a Sequestration Order to be granted against you. This means that your assets (all the money, valuables like cars and property owned by you) will be placed under control of a trustee. The trustee will negotiate  with your creditors on your behalf and convert your assets into cash to pay your Creditors. The trustee is also empowered to take a portion of your earnings to pay your Creditors.<br/><br/>\n        During the period of sequestration, you do not have legal capacity (cannot buy without permission of your trustee).<br/><br/>\n        A Sequestration Order is effective for 10 years, after which you are automatically rehabilitated. You may however apply to the court for Rehabilitation within that period. You will need to appoint an attorney to assist you.<br/><br/>\n      </div>\n    </div>\n  </div>\n</div>");
  return buffer;
  
}); });

define('appkit/templates/report/summary', [], function(){ return Ember.Handlebars.template(function anonymous(Handlebars,depth0,helpers,partials,data) {
this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Ember.Handlebars.helpers); data = data || {};
  var buffer = '', stack1, stack2, hashContexts, hashTypes, options, helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression, self=this;

function program1(depth0,data) {
  
  var buffer = '', stack1, hashTypes, hashContexts, options;
  data.buffer.push("\n        <div class=\"redBox bottomPad sameHeightBox\">\n          Total debt<br><br>\n          <div class=\"redBox redBack inTheMiddle ninetyWide verticalPad\">\n            R");
  hashTypes = {};
  hashContexts = {};
  options = {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data};
  data.buffer.push(escapeExpression(((stack1 = helpers.number || depth0.number),stack1 ? stack1.call(depth0, "summary.consumer_debt", options) : helperMissing.call(depth0, "number", "summary.consumer_debt", options))));
  data.buffer.push("\n          </div>\n          <div class=\"DivHelper\"></div>\n        </div>\n      ");
  return buffer;
  }

function program3(depth0,data) {
  
  var buffer = '', stack1, hashTypes, hashContexts, options;
  data.buffer.push("\n        <div class=\"greenBox bottomPad sameHeightBox\">\n          Total Monthly Payments\n          <div class=\"greenBox greenBack inTheMiddle ninetyWide verticalPad\">\n            R");
  hashTypes = {};
  hashContexts = {};
  options = {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data};
  data.buffer.push(escapeExpression(((stack1 = helpers.number || depth0.number),stack1 ? stack1.call(depth0, "summary.consumer_instalments", options) : helperMissing.call(depth0, "number", "summary.consumer_instalments", options))));
  data.buffer.push("\n           </div>\n        </div>\n      ");
  return buffer;
  }

function program5(depth0,data) {
  
  var buffer = '', stack1, hashContexts, hashTypes, options;
  data.buffer.push("\n        <div ");
  hashContexts = {'class': depth0};
  hashTypes = {'class': "STRING"};
  options = {hash:{
    'class': ("disabledArrears:greyBox:redBox :bottomPad :sameHeightBox")
  },contexts:[],types:[],hashContexts:hashContexts,hashTypes:hashTypes,data:data};
  data.buffer.push(escapeExpression(((stack1 = helpers['bind-attr'] || depth0['bind-attr']),stack1 ? stack1.call(depth0, options) : helperMissing.call(depth0, "bind-attr", options))));
  data.buffer.push(">\n          Total Arrears<br><br>\n          <div ");
  hashContexts = {'class': depth0};
  hashTypes = {'class': "STRING"};
  options = {hash:{
    'class': ("arrearsBoxClass :inTheMiddle :ninetyWide :verticalPad")
  },contexts:[],types:[],hashContexts:hashContexts,hashTypes:hashTypes,data:data};
  data.buffer.push(escapeExpression(((stack1 = helpers['bind-attr'] || depth0['bind-attr']),stack1 ? stack1.call(depth0, options) : helperMissing.call(depth0, "bind-attr", options))));
  data.buffer.push(">\n            R");
  hashTypes = {};
  hashContexts = {};
  options = {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data};
  data.buffer.push(escapeExpression(((stack1 = helpers.number || depth0.number),stack1 ? stack1.call(depth0, "summary.consumer_arrears", options) : helperMissing.call(depth0, "number", "summary.consumer_arrears", options))));
  data.buffer.push("\n          </div>\n        </div>\n      ");
  return buffer;
  }

function program7(depth0,data) {
  
  var buffer = '', stack1, hashContexts, hashTypes, options;
  data.buffer.push("\n        <div class=\"navBox\">\n          Fraud notices<br><br>\n          <div ");
  hashContexts = {'class': depth0};
  hashTypes = {'class': "STRING"};
  options = {hash:{
    'class': ("fraudBoxClass :inTheMiddle :ninetyWide :verticalPad")
  },contexts:[],types:[],hashContexts:hashContexts,hashTypes:hashTypes,data:data};
  data.buffer.push(escapeExpression(((stack1 = helpers['bind-attr'] || depth0['bind-attr']),stack1 ? stack1.call(depth0, options) : helperMissing.call(depth0, "bind-attr", options))));
  data.buffer.push(">\n            <img ");
  hashContexts = {'src': depth0};
  hashTypes = {'src': "ID"};
  options = {hash:{
    'src': ("fraudImgSrc")
  },contexts:[],types:[],hashContexts:hashContexts,hashTypes:hashTypes,data:data};
  data.buffer.push(escapeExpression(((stack1 = helpers['bind-attr'] || depth0['bind-attr']),stack1 ? stack1.call(depth0, options) : helperMissing.call(depth0, "bind-attr", options))));
  data.buffer.push(" class=\"textHeight\">\n            ");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers._triageMustache.call(depth0, "summary.fraud_notices", {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("\n          </div>\n        </div>\n      ");
  return buffer;
  }

function program9(depth0,data) {
  
  var buffer = '', stack1, hashContexts, hashTypes, options;
  data.buffer.push("\n        <div class=\"navBox inTheMiddle\">\n\n          Negative information<br>\n          <div ");
  hashContexts = {'class': depth0};
  hashTypes = {'class': "STRING"};
  options = {hash:{
    'class': ("negativeInfoBoxClass :inTheMiddle :ninetyWide :verticalPad")
  },contexts:[],types:[],hashContexts:hashContexts,hashTypes:hashTypes,data:data};
  data.buffer.push(escapeExpression(((stack1 = helpers['bind-attr'] || depth0['bind-attr']),stack1 ? stack1.call(depth0, options) : helperMissing.call(depth0, "bind-attr", options))));
  data.buffer.push(">\n            <img ");
  hashContexts = {'src': depth0};
  hashTypes = {'src': "ID"};
  options = {hash:{
    'src': ("negativeInfoImgSrc")
  },contexts:[],types:[],hashContexts:hashContexts,hashTypes:hashTypes,data:data};
  data.buffer.push(escapeExpression(((stack1 = helpers['bind-attr'] || depth0['bind-attr']),stack1 ? stack1.call(depth0, options) : helperMissing.call(depth0, "bind-attr", options))));
  data.buffer.push(" class=\"textHeight\">\n            ");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers._triageMustache.call(depth0, "summary.negative_information", {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("\n          </div>\n        </div>\n      ");
  return buffer;
  }

function program11(depth0,data) {
  
  var buffer = '', stack1, hashContexts, hashTypes, options;
  data.buffer.push("\n        <div class=\"navBox\">\n          Enquiries<br><br>\n          <div ");
  hashContexts = {'class': depth0};
  hashTypes = {'class': "STRING"};
  options = {hash:{
    'class': ("enquiriesBoxClass :inTheMiddle :ninetyWide :verticalPad")
  },contexts:[],types:[],hashContexts:hashContexts,hashTypes:hashTypes,data:data};
  data.buffer.push(escapeExpression(((stack1 = helpers['bind-attr'] || depth0['bind-attr']),stack1 ? stack1.call(depth0, options) : helperMissing.call(depth0, "bind-attr", options))));
  data.buffer.push(">\n            <img ");
  hashContexts = {'src': depth0};
  hashTypes = {'src': "ID"};
  options = {hash:{
    'src': ("enquiriesImgSrc")
  },contexts:[],types:[],hashContexts:hashContexts,hashTypes:hashTypes,data:data};
  data.buffer.push(escapeExpression(((stack1 = helpers['bind-attr'] || depth0['bind-attr']),stack1 ? stack1.call(depth0, options) : helperMissing.call(depth0, "bind-attr", options))));
  data.buffer.push(" class=\"textHeight\">\n            ");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers._triageMustache.call(depth0, "summary.enquiries", {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("\n          </div>\n        </div>\n      ");
  return buffer;
  }

  data.buffer.push("<div class=\"row whiteBack bottomPad topPad\">\n  <div class=\"col-xs-2\"></div>\n  <div class=\"col-xs-8 inTheMiddle\">\n    Your credit score is:<br>\n    <span ");
  hashContexts = {'class': depth0};
  hashTypes = {'class': "STRING"};
  options = {hash:{
    'class': ("status :creditScore")
  },contexts:[],types:[],hashContexts:hashContexts,hashTypes:hashTypes,data:data};
  data.buffer.push(escapeExpression(((stack1 = helpers['bind-attr'] || depth0['bind-attr']),stack1 ? stack1.call(depth0, options) : helperMissing.call(depth0, "bind-attr", options))));
  data.buffer.push(">");
  hashTypes = {};
  hashContexts = {};
  options = {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data};
  data.buffer.push(escapeExpression(((stack1 = helpers.number || depth0.number),stack1 ? stack1.call(depth0, "scoring.final_score", options) : helperMissing.call(depth0, "number", "scoring.final_score", options))));
  data.buffer.push("</span>\n  </div>\n</div>\n<div class=\"creditGauge bottomPad\">\n  <img ");
  hashContexts = {'src': depth0};
  hashTypes = {'src': "ID"};
  options = {hash:{
    'src': ("creditGaugeImageSrc")
  },contexts:[],types:[],hashContexts:hashContexts,hashTypes:hashTypes,data:data};
  data.buffer.push(escapeExpression(((stack1 = helpers['bind-attr'] || depth0['bind-attr']),stack1 ? stack1.call(depth0, options) : helperMissing.call(depth0, "bind-attr", options))));
  data.buffer.push(" style=\"max-width:100%; max-height:100%;\">\n</div>\n<div class=\"lightGreyBack smallBootstrapPadding row dblBottomPad\">\n  <div class=\"row greyTopBorder bottomPad topPad\">\n    <div class='col-xs-12 inTheMiddle'>\n    Your credit summary:\n    </div>\n  </div>\n  <div class='row topPad'>\n    <div class='col-xs-4 inTheMiddle smallBootstrapPadding'>\n      ");
  hashTypes = {};
  hashContexts = {};
  options = {hash:{},inverse:self.noop,fn:self.program(1, program1, data),contexts:[depth0],types:["STRING"],hashContexts:hashContexts,hashTypes:hashTypes,data:data};
  stack2 = ((stack1 = helpers['link-to'] || depth0['link-to']),stack1 ? stack1.call(depth0, "report.accounts", options) : helperMissing.call(depth0, "link-to", "report.accounts", options));
  if(stack2 || stack2 === 0) { data.buffer.push(stack2); }
  data.buffer.push("\n    </div>\n    <div class='col-xs-4 inTheMiddle smallBootstrapPadding'>\n      ");
  hashTypes = {};
  hashContexts = {};
  options = {hash:{},inverse:self.noop,fn:self.program(3, program3, data),contexts:[depth0],types:["STRING"],hashContexts:hashContexts,hashTypes:hashTypes,data:data};
  stack2 = ((stack1 = helpers['link-to'] || depth0['link-to']),stack1 ? stack1.call(depth0, "report.accounts", options) : helperMissing.call(depth0, "link-to", "report.accounts", options));
  if(stack2 || stack2 === 0) { data.buffer.push(stack2); }
  data.buffer.push("\n    </div>\n    <div class='col-xs-4 inTheMiddle smallBootstrapPadding'>\n      ");
  hashContexts = {'disabledWhen': depth0};
  hashTypes = {'disabledWhen': "ID"};
  options = {hash:{
    'disabledWhen': ("disabledArrears")
  },inverse:self.noop,fn:self.program(5, program5, data),contexts:[depth0],types:["STRING"],hashContexts:hashContexts,hashTypes:hashTypes,data:data};
  stack2 = ((stack1 = helpers['link-to'] || depth0['link-to']),stack1 ? stack1.call(depth0, "report.accounts", options) : helperMissing.call(depth0, "link-to", "report.accounts", options));
  if(stack2 || stack2 === 0) { data.buffer.push(stack2); }
  data.buffer.push("\n    </div>\n  </div>\n\n  <div class=\"row dottedBottomDark bottomPad smallMargin lightGreyBack\">\n    <div class='col-xs-12 inTheMiddle'>\n    </div>\n  </div>\n\n  <div class='row topPad'>\n    <div class='col-xs-4 inTheMiddle'>\n      ");
  hashTypes = {};
  hashContexts = {};
  options = {hash:{},inverse:self.noop,fn:self.program(7, program7, data),contexts:[depth0],types:["STRING"],hashContexts:hashContexts,hashTypes:hashTypes,data:data};
  stack2 = ((stack1 = helpers['link-to'] || depth0['link-to']),stack1 ? stack1.call(depth0, "report.fraudulent", options) : helperMissing.call(depth0, "link-to", "report.fraudulent", options));
  if(stack2 || stack2 === 0) { data.buffer.push(stack2); }
  data.buffer.push("\n    </div>\n    <div class='col-xs-4 dottedSidesDark'>\n      ");
  hashTypes = {};
  hashContexts = {};
  options = {hash:{},inverse:self.noop,fn:self.program(9, program9, data),contexts:[depth0],types:["STRING"],hashContexts:hashContexts,hashTypes:hashTypes,data:data};
  stack2 = ((stack1 = helpers['link-to'] || depth0['link-to']),stack1 ? stack1.call(depth0, "report.negative", options) : helperMissing.call(depth0, "link-to", "report.negative", options));
  if(stack2 || stack2 === 0) { data.buffer.push(stack2); }
  data.buffer.push("\n    </div>\n    <div class='col-xs-4 inTheMiddle'>\n      ");
  hashTypes = {};
  hashContexts = {};
  options = {hash:{},inverse:self.noop,fn:self.program(11, program11, data),contexts:[depth0],types:["STRING"],hashContexts:hashContexts,hashTypes:hashTypes,data:data};
  stack2 = ((stack1 = helpers['link-to'] || depth0['link-to']),stack1 ? stack1.call(depth0, "report.enquiries", options) : helperMissing.call(depth0, "link-to", "report.enquiries", options));
  if(stack2 || stack2 === 0) { data.buffer.push(stack2); }
  data.buffer.push("\n    </div>\n  </div>\n</div>\n\n<!-- Modal -->\n<div class=\"modal fade\" id=\"myModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">\n  <div class=\"modal-dialog\">\n    <div class=\"modal-content\">\n      <div class=\"modal-header\">\n        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>\n        <h4 class=\"modal-title\" id=\"myModalLabel\">Your Snapshot</h4>\n      </div>\n      <div class=\"modal-body\">\n        Your credit score is a rating out of 1 000 given to you by XDS. Each bureau calculates your credit score with their own rating system, so your score might differ from one bureau to the other.<br><br>\n\n        Your score indicates the credit risk associated with you and takes into consideration all your past and current credit activities and repayment behaviour. The higher your score, the lower\n        the risk you are to Credit Providers.<br><br>\n\n        <b>To improve your credit score:</b>\n        <ul>\n          <li>Monitor your credit report regularly</li>\n          <li>Keep your credit applications to a minimum</li>\n          <li>Use less than 30% of credit available to you on a monthly basis</li>\n          <li>Pay your bills on time</li>\n          <li>Close unused accounts</li>\n        </ul>\n      </div>\n    </div>\n  </div>\n</div>");
  return buffer;
  
}); });

define('appkit/templates/report/tasks', [], function(){ return Ember.Handlebars.template(function anonymous(Handlebars,depth0,helpers,partials,data) {
this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Ember.Handlebars.helpers); data = data || {};
  


  data.buffer.push("<div>\n  <img src='img/full_page_images/Credit-Crunch-Tasks-iphone-3.png' class='fullWidth'>\n</div>\n<div class='fullPageImageSpacer'></div>\n");
  
}); });