define("appkit/adapters/application",
  [],
  function() {
    "use strict";
    var FixtureAdapter = DS.FixtureAdapter.extend();

    return FixtureAdapter;
  });
define("appkit/app",
  ["resolver","appkit/utils/register_components"],
  function(Resolver, registerComponents) {
    "use strict";

    var App = Ember.Application.extend({
      LOG_ACTIVE_GENERATION: true,
      LOG_MODULE_RESOLVER: true,
      LOG_TRANSITIONS: true,
      LOG_TRANSITIONS_INTERNAL: true,
      LOG_VIEW_LOOKUPS: true,
      modulePrefix: 'appkit', // TODO: loaded via config
      Resolver: Resolver
    });

    App.initializer({
      name: 'Register Components',
      initialize: function(container, application) {
        registerComponents(container);
      }
    });

    Ember.Handlebars.helper('number', function(value, options) {
      var escaped = Handlebars.Utils.escapeExpression(value);
      return new Handlebars.SafeString(escaped.substr(0, escaped.indexOf(".")));
    });

    Ember.Handlebars.helper('xxxIfNoValue', function(value, options) {
      if (value==='No' || value === 'Unknown' || value === 'No Access' || value === '') {
        return 'xxxxx';
      }
      return value;
    });

    Ember.Handlebars.helper('not-blank', function(value, options) {
      var escaped = Handlebars.Utils.escapeExpression(value);
      if (escaped) {
        return escaped;
      } else {
        return new Handlebars.SafeString('&nbsp');
      }
    });

    Ember.Handlebars.helper('monthName', function(context, month) {
      return context.get('report.payment_headers.'+month);
    });

    Ember.Handlebars.helper('paymentHistory', function(context, month) {
      var symbol = context.get('content.payment_history.'+month);

      return symbol +' '+context.get('report.payment_history_types.'+symbol);
    });

    var getPaymentHistoryInfo = function(context, month, isWhite) {

      var symbol = context.get('content.payment_history.'+month);
      var monthsAgo = window.App.report.get('payment_headers.'+month).toLowerCase().capitalize();

      if (isWhite) {
        var imgSrc = 'assets/img/accountStatusWhite/';
      } else {
        var imgSrc = 'assets/img/paymentHistory/';
      }

      var popoverInfo = '';

      switch (symbol) {
        case '#':
          imgSrc+='no-info';
          popoverInfo = 'No Information Available';
          break;
        case '0':
          imgSrc+='up-to-date';
          popoverInfo = 'Account Up To Date';
          break;
        case 'P':
          imgSrc+='paid-up';
          popoverInfo = 'Paid Up';
          break;
        case 'W':
          imgSrc+='bad-debt-written-off';
          popoverInfo = 'Bad Debt Written Off';
          break;
        case '*':
          imgSrc+='repeat-previous-month';
          popoverInfo = 'Repeat Of Previous Months Code';
          break;
        default:
          imgSrc+=symbol+'-mo-arrears';
          popoverInfo = 'Number Of Months In Arrears';

      }

      if (isWhite) {
        imgSrc+='-white.png';
      } else {
        imgSrc+='.png';
      }

      return {
        imgSrc: imgSrc,
        popoverInfo: popoverInfo,
        monthsAgo: monthsAgo
      };
    };

    var modalHtml = function(info,month){
      console.log(month);
      var modalId = 'paymentIcon'+month;
      return new Handlebars.SafeString(
        '<img src="'+info.imgSrc+'" data-toggle="modal" data-target="#'+modalId+'" height="30"><br>'+
        '<span class="smallText">'+info.monthsAgo+'</span>'+
        '<div class="modal fade" id="'+modalId+'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">'+
        '<div class="modal-dialog ninetyWide">'+
        '<div class="modal-content">'+
        '<div class="modal-header">'+
        '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>'+
        '&nbsp;'+
        '</div>'+
        '<div class="modal-body">'+
        '<img src="'+info.imgSrc+'">&nbsp;&nbsp;'+
        info.popoverInfo+
        '</div>'+
        '</div>'+
        '</div>'+
        '</div>');
    };

    // NOTE: div name collisions if generate both paymentHistoryImgs and paymentHistoryWhiteImgs
    Ember.Handlebars.helper('paymentHistoryImg', function(context, month) {
      var info = getPaymentHistoryInfo(context, month, false);
      return modalHtml(info,month);
    });

    Ember.Handlebars.helper('paymentHistoryWhiteImg', function(context, month) {
      var info = getPaymentHistoryInfo(context, month, true);
      return new modalHtml(info,month);
    });

    Ember.Handlebars.helper('currentMonthPaymentIcon', function(context, month) {
      var info = getPaymentHistoryInfo(context, month, true);
      return new Handlebars.SafeString('<img src="'+info.imgSrc+'" data-toggle="modal" data-target="#paymentIcon'+month+'">');
    });


    return App;
  });
define("appkit/components/pretty-color",
  [],
  function() {
    "use strict";
    var PrettyColor = Ember.Component.extend({
        classNames: ['pretty-color'],
        attributeBindings: ['style'],
        style: function(){
          return 'color: ' + this.get('name') + ';';
        }.property('name')
    });


    return PrettyColor;
  });
define("appkit/controllers/account",
  [],
  function() {
    "use strict";
    var AccountController = Ember.ObjectController.extend({

      accountStatusImgSrc: function() {
        if (this.get('isInArrears')) {
          return 'assets/img/accounts/allert.png';
        } else {
          return 'assets/img/accounts/up-to-date.png'
        }
      }.property('isInArrears'),

      isInArrears: function() {
        if (this.get('arrears_amount')==='0.0000') return false;
        return true;
      }.property('arrears_amount')

    });

    return AccountController;
  });
define("appkit/controllers/activate",
  ["appkit/models/credit_report"],
  function(CreditReport) {
    "use strict";

    var ActivateController = Ember.Controller.extend({

      needs: ['application'],

      actions: {
        activate: function() {

          var self = this;

          window.localStorage.removeItem("userKey");

          //System key
          $.ajaxSetup({
            headers: { 'apikey': '9fbc7c25-6421-4e51-90a5-acb92216d229' }
          });

          var voucher = this.get('voucher');
          var id = this.get('id');

          return $.get ("http://196.14.133.186/api/voucher/claim/"+voucher+"/id/"+id).then(
            function (response) {
              window.localStorage.setItem("userKey", response);
              self.transitionToRoute('passcode');
              console.log(window.localStorage.getItem("userKey"));
            }, function(response) {
              console.log('failed retrieved voucher');
              self.set('errors', 'Sorry! Invalid Voucher / ID combination')
            });



        }
      }

    });

    return ActivateController;
  });
define("appkit/controllers/application",
  [],
  function() {
    "use strict";
    var ApplicationController = Ember.Controller.extend({

      passcodeVerified: null,
      backRoute: null,
      hideSplashCount: 0,

      hideSplashScreen: function() {
        console.log('Hide splash count: '+this.get('hideSplashCount'));
        if (this.get('hideSplashCount')==3 && window.device) {
          if (navigator && navigator.splashscreen) {
            navigator.splashscreen.hide();
          }
        }
      }.observes('hideSplashCount'),

      actions: {
        toggleNav: function(){

          var menuLeft = $('.pushmenu-left');
          var navList = $('#nav_list');

          navList.toggleClass('active');
          $('.pushmenu-push').toggleClass('pushmenu-push-toright');
          menuLeft.toggleClass('pushmenu-open');

        },

        gotoBackRoute: function() {
          this.transitionToRoute(this.get('backRoute'));
          this.set('backRoute', null);
        }
      }
    })

    return ApplicationController;
  });
define("appkit/controllers/index",
  [],
  function() {
    "use strict";
    var IndexController = Ember.ObjectController.extend({

      accounts: ['Edgars', 'Capitec', 'Telkom', '8ta'],
      selectedAccount: null,

      register: function() {
          this.transitionToRoute('verify');
        }
    });

    return IndexController;
  });
define("appkit/controllers/passcode",
  ["appkit/models/credit_report"],
  function(CreditReport) {
    "use strict";

    var PasscodeController = Ember.Controller.extend({

      needs: ['application'],
      errors: {},

      hasErrors: function(){
        return (this.get('errors.length') || this.get('errors.mismatch'));
      }.property('errors.length','errors.mismatch'),

      actions: {
        verifyAndCreatePasscode: function() {
          this.set('errors',{});
          if ((this.get('passcode')).length < 4) {
            this.set('errors.length', 'Pass code must be at least 4 numbers');
          }
          if (this.get('passcode') != this.get('confirmPasscode')) {
            this.set('errors.mismatch','Pass codes do not match');
          }
          if (!this.get('hasErrors')) {
            window.localStorage.setItem("userPasscode",this.get('passcode'));
            this.set('controllers.application.passcodeVerified',true);
            this.transitionToRoute('report.summary');
          }
        }
      }

    });
    return PasscodeController;
  });
define("appkit/controllers/passcode_login",
  [],
  function() {
    "use strict";
    var PasscodeLoginController = Ember.Controller.extend({

      needs: ['application'],

      actions: {
        verifyPasscode: function() {
          var existingPasscode = window.localStorage.getItem("userPasscode");
          if (existingPasscode == this.get('passcode')) {
            this.set('controllers.application.passcodeVerified',true);
            this.transitionToRoute('report.summary');
          } else {
            this.set('errors','Incorrect passcode');
          }
        },

        clearPasscode: function() {
          window.localStorage.removeItem('userPasscode');
          this.transitionToRoute('activate');
        }
      }

    });

    return PasscodeLoginController;
  });
define("appkit/controllers/report",
  [],
  function() {
    "use strict";
    var ReportController = Ember.ObjectController.extend({

      needs: ['application']

    });
    return ReportController;
  });
define("appkit/controllers/report/account",
  [],
  function() {
    "use strict";
    var ReportAccountController = Ember.ObjectController.extend({

      needs: ['report'],
      report: Ember.computed.alias('controllers.report.content'),
      months: Ember.A(['01', '02', '03', '04', '05', '06']),

      accountType: function() {

        return this.get('report.consumer_account_types.'+this.get('content.type'));

      }.property(),

      inArrears: function() {
        if ( this.get('status')=='In Arrears') {
          return 'redBack redBox';
        } else {
          return 'greenBack greenBox';
        }
      }.property()





    })

    return ReportAccountController;
  });
define("appkit/controllers/report/accounts",
  [],
  function() {
    "use strict";
    var ReportAccountsController = Ember.ObjectController.extend({

      showLoanAccounts: function() {
        return this.get('content.loan_accounts.length') >0;
      }.property(),

      showCreditAccounts: function() {
        return this.get('content.consumer_accounts.length') >0;
      }.property()




    })

    return ReportAccountsController;
  });
define("appkit/controllers/report/enquiries",
  [],
  function() {
    "use strict";
    var con =  Ember.ObjectController.extend({
      needs: ['report'],

      moreEnquiriesAvailable: function() {
        return this.get('content.length') < this.get('controllers.report.content.enquiries.length');
      }.property()

    });

    return con;
  });
define("appkit/controllers/report/negative",
  [],
  function() {
    "use strict";
    var ReportNegativeController = Ember.ObjectController.extend({

      disabledRehabLink: Ember.computed.equal('rehabilitation_orders.length', 0),
      disabledSequestrationsLink: Ember.computed.equal('sequestrations.length', 0),
      disabledAdverseLink: Ember.computed.equal('adverse_defaults.length', 0),
      disabledDefaultListingsLink: Ember.computed.equal('default_listings.length', 0),
      disabledJudgementsLink: Ember.computed.equal('judgements.length', 0),
      disabledAdminOrdersLink: Ember.computed.equal('admin_orders.length', 0),

      redOrGrey: function(propertyName) {
        if (this.get(propertyName)>0) {
          return 'redBox redBack';
        }
        return 'greyBox greyBack';
      },

      adverseClass: function() {
        return this.redOrGrey('adverse_defaults.length');
      }.property(),

      defaultListingsClass: function() {
        return this.redOrGrey('default_listings.length');
      }.property(),

      sequestrationsClass: function() {
        return this.redOrGrey('sequestrations.length');
      }.property(),

      rehabilitationOrdersClass: function() {
        return this.redOrGrey('rehabilitation_orders.length');
      }.property(),

      adminOrdersClass: function() {
        return this.redOrGrey('admin_orders.length');
      }.property(),

      judgementsClass: function() {
        return this.redOrGrey('judgements.length');
      }.property(),

      debtReviewStatusClass: function() {
        return this.redOrGrey('judgements.length');
      }.property(),

      showDebtReviewStatus: function() {
        return this.get('debt_review.length')==undefined;
      }.property()

    });

    return ReportNegativeController;
  });
define("appkit/controllers/report/summary",
  [],
  function() {
    "use strict";
    var SummaryController = Ember.ObjectController.extend({

      needs: ['application'], //applicaiton reference used by the view

      disabledArrears: Ember.computed.equal('summary.consumer_accounts_in_arrears', '0'),
      disabledFraud: Ember.computed.equal('summary.fraud_notices', 0),
      disabledNegativeInfo: Ember.computed.equal('summary.negative_information', 0),
      disabledEnquiries: Ember.computed.equal('summary.enquiries', 0),

      creditGaugeImageSrc: function() {
        return 'assets/img/snapshot/gauge/'+this.get('status')+'.png';
      }.property('status'),

      redOrGreyBox: function(propName) {
        if (this.get(propName)) {
          return 'greyBox greyBack';
        }
        return 'redBox redBack';
      },

      arrearsBoxClass: function() {
        return this.redOrGreyBox('disabledArrears');
      }.property('disabledArrears'),

      fraudBoxClass: function() {
        return this.redOrGreyBox('disabledFraud');
      }.property(),

      enquiriesBoxClass: function() {
        return this.redOrGreyBox('disabledEnquiries');
      }.property(),

      negativeInfoBoxClass: function() {
        return this.redOrGreyBox('disabledNegativeInfo');
      }.property(),

      negativeInfoImgSrc: function() {
        if (this.get('disabledNegativeInfo')) {
          return 'assets/img/snapshot/icons/negative-info-gray-icon.png'
        } else {
          return 'assets/img/snapshot/icons/negative-info-red-icon.png'
        }
      }.property(),

      fraudImgSrc: function() {
        if (this.get('disabledFraud')) {
          return 'assets/img/snapshot/icons/fraud-gray-icon.png'
        } else {
          return 'assets/img/snapshot/icons/fraud-red-icon.png'
        }
      }.property(),

      enquiriesImgSrc: function() {
        if (this.get('disabledEnquiries')) {
          return 'assets/img/snapshot/icons/enquiries-gray-icon.png'
        } else {
          return 'assets/img/snapshot/icons/enquiries-red-icon.png'
        }
      }.property()

    });

    return SummaryController;
  });
define("appkit/helpers/reverse-word",
  [],
  function() {
    "use strict";
    var reverseWord = Ember.Handlebars.makeBoundHelper(function(word) {
      return word.split('').reverse().join('');
    });


    return reverseWord;
  });
define("appkit/mixins/equal_box_heights",
  [],
  function() {
    "use strict";
    var EqualBoxHeights = Ember.Mixin.create({
      equalizeBoxHeights: function(sameHeightClassName) {
        var maxHeight = 0;
        var boxes;
        if (sameHeightClassName) {
          boxes = this.$(sameHeightClassName);
        } else {
          boxes = this.$('.sameHeightBox');
        }
        boxes.each(function(index, box) {
          if ($(box).height() > maxHeight) {

            maxHeight = $(box).innerHeight();
          }
        });
        console.log('setting height to: '+maxHeight);
        boxes.css('height', maxHeight);
      }
    });

    return EqualBoxHeights;
  });
define("appkit/models/action_plan",
  [],
  function() {
    "use strict";
    var ActionPlan = Ember.Object.extend({

    });

    return ActionPlan;
  });
define("appkit/models/credentials",
  [],
  function() {
    "use strict";
    //require
    var Credentials = Ember.Object.extend({
      userName: '',
      password: '',
      id: null
    });
    return Credentials;
  });
define("appkit/models/credit_report",
  [],
  function() {
    "use strict";
    var CreditReport = Ember.Object.extend({

      isLoaded: false,

      status: function() {
        var score = this.get('scoring.final_score');
        if (score<660) {
          return 'poor';
        } else if (score < 740) {
          return 'average';
        } else if (score < 840) {
          return 'good';
        } else {
          return 'excellent';
        }


      }.property('scoring.final_score')

    });

    return CreditReport;
  });
define("appkit/router",
  [],
  function() {
    "use strict";
    var Router = Ember.Router.extend(); // ensure we don't share routes between all Router instances

    Router.map(function() {
      this.route('component-test');
      this.route('helper-test');
      this.route('login');
      this.route('activate');
      this.route('passcode');
      this.route('passcode_login');
      this.route('registration');
      this.route('verify');
      this.route('verified');
      this.route('action');
      this.resource('report', function() {
        this.route('enquiries');
        this.route('fraudulent');
        this.route('negative');
        this.route('summary');
        this.route('contact');
        this.route('profile');
        this.route('accounts');
        this.route('account', {path: '/account/:account_number'});
        this.route('notifications');
        this.route('tasks');
        this.route('adverse');
        this.route('default_listings');
        this.route('judgements');
        this.route('admin_orders');
        this.route('sequestrations');
        this.route('rehab_orders');
        this.route('debt_review');
      });
      this.route('contact');
      this.route('help');

      // this.resource('posts', function() {
      //   this.route('new');
      // });
    });


    return Router;
  });
define("appkit/routes/action",
  ["appkit/models/action_plan"],
  function(ActionPlan) {
    "use strict";
    var ActionRoute = Ember.Route.extend({

      loadedModel: null,

      beforeModel: function () {
        var actionPlan = ActionPlan.create({});
        var self = this;

        var userKey = window.localStorage.getItem("userKey");
        $.ajaxSetup({
    //      headers: { 'apikey': userKey }
          headers: { 'apikey': '880a06eb-0d40-4660-a18e-a32a8500b78f' }
        });

        var id = '9912315813089';
        if (this.controllerFor('application').get('alternateId')) {
          id = this.controllerFor('application').get('alternateId');
        };

        return $.getJSON("http://196.14.133.186/api/content/month").then(
          function (response) {
            actionPlan.setProperties(response.Content);
            actionPlan.set('contentForMonth', response.Content.content);
            self.set('loadedModel', actionPlan);
          });
      },

      model: function() {
        return this.get('loadedModel');
      },

      activate: function() {
        this.controllerFor('application').set('navLocation', 'Action Plan');
        this.controllerFor('application').set('navImage', 'assets/img/menuNavIcons/action-plan-dark.png');
      }

    });

    return ActionRoute;
  });
define("appkit/routes/activate",
  [],
  function() {
    "use strict";
    var ActivateRoute = Ember.Route.extend({
      activate: function() {
        this.controllerFor('application').set('navLocation', 'Activation');
      }
    });

    return ActivateRoute;
  });
define("appkit/routes/application",
  [],
  function() {
    "use strict";
    var ApplicationRoute = Ember.Route.extend({
      redirect: function() {
        var userPasscode = window.localStorage.getItem("userPasscode");
        if (Ember.isEmpty(userPasscode)) {
          this.transitionTo('activate');
        } else {
          this.transitionTo('passcode_login');
        }
      }
    });

    return ApplicationRoute;
  });
define("appkit/routes/component_test",
  [],
  function() {
    "use strict";
    var ComponentTestRoute = Ember.Route.extend({
      model: function() {
        return ['purple', 'green', 'orange'];
      }
    });



    return ComponentTestRoute;
  });
define("appkit/routes/contact",
  [],
  function() {
    "use strict";
    var ContactRoute = Ember.Route.extend({

      activate: function() {
        this.controllerFor('application').set('navLocation', 'Contact');
        this.controllerFor('application').set('navImage', 'assets/img/menuNavIcons/contact-dark.png');
      }





    });


    return ContactRoute;
  });
define("appkit/routes/helper_test",
  [],
  function() {
    "use strict";
    var HelperTestRoute = Ember.Route.extend({
      model: function() {
        return {
          name: "rebmE"
        };
      }
    });


    return HelperTestRoute;
  });
define("appkit/routes/index",
  [],
  function() {
    "use strict";
    var IndexRoute = Ember.Route.extend({

      redirect: function() {
        this.transitionTo('report.summary');
      }
    });


    return IndexRoute;
  });
define("appkit/routes/passcode",
  [],
  function() {
    "use strict";
    var PasscodeRoute = Ember.Route.extend({

      activate: function() {
        this.controllerFor('application').set('navLocation', 'Pass Code Set Up');
      }
    });

    return PasscodeRoute;
  });
define("appkit/routes/passcode_login",
  [],
  function() {
    "use strict";
    var PasscodeLoginRoute = Ember.Route.extend({

      model: function() {
        return {
          firstName: window.localStorage.getItem('userFirstName')
        };
      },

      activate: function() {
        this.controllerFor('application').set('navLocation', 'Pass Code Login');
      }
    });


    return PasscodeLoginRoute;
  });
define("appkit/routes/report",
  ["appkit/models/credit_report"],
  function(CreditReport) {
    "use strict";
    var ReportRoute = Ember.Route.extend({

      loadedModel: null,

      beforeModel: function () {
        var report = CreditReport.create({scoring: {final_score: 700}});
        var self = this;

        var userKey = window.localStorage.getItem("userKey");
        $.ajaxSetup({
          headers: { 'apikey': userKey }
    //      headers: { 'apikey': '880a06eb-0d40-4660-a18e-a32a8500b78f' }
        });

        return $.getJSON("http://196.14.133.186/api/credit/get").then(
          function (response) {
            self.controllerFor('application').incrementProperty('hideSplashCount');
            console.log('retrieved report');
            report.setProperties(response);
            report.set('isLoaded', true);
            self.set('loadedModel', report);
            if (response.personal_details.first_name) {
              var firstName = response.personal_details.first_name.toLowerCase().capitalize();
              window.localStorage.setItem("userFirstName", firstName);
            }

            window.App.report = report;
          });
      },

      model: function() {
        return this.get('loadedModel');
      }

    });

    return ReportRoute;
  });
define("appkit/routes/report/account",
  [],
  function() {
    "use strict";
    var ReportAccountRoute = Ember.Route.extend({

      model: function(params) {

        var getAccount = function(report) {
          var account = report.get('consumer_accounts').findBy('account_number', params.account_number);
          return account;
        }

        var report = this.modelFor('report');
        if (report.get('isLoaded')) return getAccount(report);

        var promise = new Ember.RSVP.Promise(function(resolve, reject){
          report.addObserver('isLoaded', function() {
            if (report.get('isLoaded')) {
              report.removeObserver('isLoaded', this);
              resolve(getAccount(report));
            }
          });

        });
       return promise;

      },

      activate: function() {
        this.controllerFor('application').set('backRoute', 'report.accounts');
        this.controllerFor('application').set('navLocation', 'Your Payment History');
        this.controllerFor('application').set('navImage', 'assets/img/menuNavIcons/accounts-dark.png');
      },

      deactivate: function() {
        this.controllerFor('application').set('backRoute', null);
      }



    });

    return ReportAccountRoute;
  });
define("appkit/routes/report/accounts",
  [],
  function() {
    "use strict";
    var ReportAccountsRoute = Ember.Route.extend({

      //$('.progress-value').width('60%')
    //  http://jsfiddle.net/danield770/nFvUu/3/

      model: function() {
        return this.modelFor('report');
      },

      activate: function() {
        this.controllerFor('application').set('navLocation', 'Your Accounts');
        this.controllerFor('application').set('navImage', 'assets/img/menuNavIcons/accounts-dark.png');
      }





    });

    return ReportAccountsRoute;
  });
define("appkit/routes/report/admin_orders",
  [],
  function() {
    "use strict";
    var ReportAdminOrdersRoute = Ember.Route.extend({

      model: function() {
        return this.modelFor('report');
      },

      activate: function() {
        this.controllerFor('application').set('backRoute', 'report.negative');
        this.controllerFor('application').set('navLocation', 'Admin Orders');
        this.controllerFor('application').set('navImage', 'assets/img/menuNavIcons/negative-info-dark.png');
      },

      deactivate: function() {
        this.controllerFor('application').set('backRoute', null);
      }

    });

    return ReportAdminOrdersRoute;
  });
define("appkit/routes/report/adverse",
  [],
  function() {
    "use strict";
    var ReportAdverseRoute = Ember.Route.extend({

      model: function() {
        return this.modelFor('report');
      },

      activate: function() {
        this.controllerFor('application').set('backRoute', 'report.negative');
        this.controllerFor('application').set('navLocation', 'Adverse / Defaults');
        this.controllerFor('application').set('navImage', 'assets/img/menuNavIcons/negative-info-dark.png');
      },

      deactivate: function() {
        this.controllerFor('application').set('backRoute', null);
      }

    });

    return ReportAdverseRoute;
  });
define("appkit/routes/report/debt_review",
  [],
  function() {
    "use strict";
    var ReportDebtReviewRoute = Ember.Route.extend({

      model: function() {
        return this.modelFor('report');
      },

      activate: function() {
        this.controllerFor('application').set('backRoute', 'report.negative');
        this.controllerFor('application').set('navLocation', 'Debt Review');
        this.controllerFor('application').set('navImage', 'assets/img/menuNavIcons/negative-info-dark.png');
      },

      deactivate: function() {
        this.controllerFor('application').set('backRoute', null);
      }

    });

    return ReportDebtReviewRoute;
  });
define("appkit/routes/report/default_listings",
  [],
  function() {
    "use strict";
    var ReportDefaultListingsRoute = Ember.Route.extend({

      model: function() {
        return this.modelFor('report');
      },

      activate: function() {
        this.controllerFor('application').set('backRoute', 'report.negative');
        this.controllerFor('application').set('navLocation', 'Default Listings');
        this.controllerFor('application').set('navImage', 'assets/img/menuNavIcons/negative-info-dark.png');
      },

      deactivate: function() {
        this.controllerFor('application').set('backRoute', null);
      }

    });

    return ReportDefaultListingsRoute;
  });
define("appkit/routes/report/enquiries",
  [],
  function() {
    "use strict";
    var ReportEnquiriesRoute = Ember.Route.extend({

      model: function() {
        var report = this.modelFor('report');

        if (report.get('enquiries') && report.get('enquiries').length && report.get('enquiries').length <20) {
          return report.get('enquiries');
        }
        return report.get('enquiries').slice(0, 50);
      },

      activate: function() {
        this.controllerFor('application').set('navLocation', 'Enquiries');
        this.controllerFor('application').set('navImage', 'assets/img/menuNavIcons/enquiries-dark.png');
      }

    });

    return ReportEnquiriesRoute;
  });
define("appkit/routes/report/fraudulent",
  [],
  function() {
    "use strict";
    var ReportFraudulentRoute = Ember.Route.extend({

      //$('.progress-value').width('60%')
    //  http://jsfiddle.net/danield770/nFvUu/3/

      model: function() {
        return this.modelFor('report');
      },

      activate: function() {
        this.controllerFor('application').set('navLocation', 'Fraudulent Activities');
        this.controllerFor('application').set('navImage', 'assets/img/menuNavIcons/fruad-dark.png');
      }





    });

    return ReportFraudulentRoute;
  });
define("appkit/routes/report/judgements",
  [],
  function() {
    "use strict";
    var ReportJudgementsRoute = Ember.Route.extend({

      model: function() {
        return this.modelFor('report');
      },

      activate: function() {
        this.controllerFor('application').set('backRoute', 'report.negative');
        this.controllerFor('application').set('navLocation', 'Judgements');
        this.controllerFor('application').set('navImage', 'assets/img/menuNavIcons/negative-info-dark.png');
      },

      deactivate: function() {
        this.controllerFor('application').set('backRoute', null);
      }

    });

    return ReportJudgementsRoute;
  });
define("appkit/routes/report/negative",
  [],
  function() {
    "use strict";
    var ReportNegativeRoute = Ember.Route.extend({

      model: function() {
        return this.modelFor('report');
      },

      activate: function() {
        this.controllerFor('application').set('navLocation', 'Your Negative Info');
        this.controllerFor('application').set('navImage', 'assets/img/menuNavIcons/negative-info-dark.png');
      }

    });

    return ReportNegativeRoute;
  });
define("appkit/routes/report/profile",
  [],
  function() {
    "use strict";
    var ReportProfileRoute = Ember.Route.extend({

      //$('.progress-value').width('60%')
    //  http://jsfiddle.net/danield770/nFvUu/3/

      model: function() {
        return this.modelFor('report');
      },

      activate: function() {
        this.controllerFor('application').set('navLocation', 'Your Personal Info');
        this.controllerFor('application').set('navImage', 'assets/img/menuNavIcons/personal-info-dark.png');
      }





    });


    return ReportProfileRoute;
  });
define("appkit/routes/report/rehab_orders",
  [],
  function() {
    "use strict";
    var ReportRehabOrdersRoute = Ember.Route.extend({

      model: function() {
        return this.modelFor('report');
      },

      activate: function() {
        this.controllerFor('application').set('backRoute', 'report.negative');
        this.controllerFor('application').set('navLocation', 'Rehabilitation Orders');
        this.controllerFor('application').set('navImage', 'assets/img/menuNavIcons/negative-info-dark.png');
      },

      deactivate: function() {
        this.controllerFor('application').set('backRoute', null);
      }

    });

    return ReportRehabOrdersRoute;
  });
define("appkit/routes/report/sequestrations",
  [],
  function() {
    "use strict";
    var ReportSequestrationsRoute = Ember.Route.extend({

      model: function() {
        return this.modelFor('report');
      },

      activate: function() {
        this.controllerFor('application').set('backRoute', 'report.negative');
        this.controllerFor('application').set('navLocation', 'Sequestrations');
        this.controllerFor('application').set('navImage', 'assets/img/menuNavIcons/negative-info-dark.png');
      },

      deactivate: function() {
        this.controllerFor('application').set('backRoute', null);
      }

    });

    return ReportSequestrationsRoute;
  });
define("appkit/routes/report/summary",
  [],
  function() {
    "use strict";
    var ReportSummaryRoute = Ember.Route.extend({

      //$('.progress-value').width('60%')
    //  http://jsfiddle.net/danield770/nFvUu/3/

      model: function() {
        return this.modelFor('report');
      },

      activate: function() {
        this.controllerFor('application').set('navLocation', 'Your Snapshot');
        this.controllerFor('application').set('navImage', 'assets/img/menuNavIcons/snapshot-dark.png');
      }




    });

    return ReportSummaryRoute;
  });
define("appkit/utils/register_components",
  [],
  function() {
    "use strict";
    /* global requirejs */
    /* global require */

    function registerComponents(container) {
      var seen = requirejs._eak_seen;
      var templates = seen, match;
      if (!templates) { return; }

      for (var prop in templates) {
        if (match = prop.match(/templates\/components\/(.*)$/)) {
          require(prop, null, null, true);
          registerComponent(container, match[1]);
        }
      }
    }


    function registerComponent(container, name) {
      Ember.assert("You provided a template named 'components/" + name + "', but custom components must include a '-'", name.match(/-/));

      var fullName         = 'component:' + name,
          templateFullName = 'template:components/' + name;

      container.injection(fullName, 'layout', templateFullName);

      var Component = container.lookupFactory(fullName);

      if (!Component) {
        container.register(fullName, Ember.Component);
        Component = container.lookupFactory(fullName);
      }

      Ember.Handlebars.helper(name, Component);
    }


    return registerComponents;
  });
define("appkit/views/application",
  [],
  function() {
    "use strict";
    var ApplicationView = Ember.View.extend({

      didInsertElement: function () {

        var navLinks = $('.pushmenu-left a');
        var self = this;

        navLinks.click(function () {
          self.triggerAction({
            action: 'toggleNav',
            target: self.get('controller')
          });
        });

        document.addEventListener("deviceready", onDeviceReady, false);

        //All credit to Shawn Olson http://www.shawnolson.net/a/503/altering_css_class_attributes_with_javascript.html
        function changeCSS(theClass,element,value) {
          //Last Updated on July 4, 2011
          //documentation for this script at
          //http://www.shawnolson.net/a/503/altering-css-class-attributes-with-javascript.html
          var cssRules;
          for (var S = 0; S < document.styleSheets.length; S++){
            try{
              document.styleSheets[S].insertRule(theClass+' { '+element+': '+value+'; }',document.styleSheets[S][cssRules].length);
            } catch(err){
              try{document.styleSheets[S].addRule(theClass,element+': '+value+';');
              }catch(err){
                try{
                  if (document.styleSheets[S]['rules']) {
                    cssRules = 'rules';
                  } else if (document.styleSheets[S]['cssRules']) {
                    cssRules = 'cssRules';
                  } else {
                    //no rules found... browser unknown
                  }
                  for (var R = 0; R < document.styleSheets[S][cssRules].length; R++) {
                    if (document.styleSheets[S][cssRules][R].selectorText == theClass) {
                      if(document.styleSheets[S][cssRules][R].style[element]){
                        document.styleSheets[S][cssRules][R].style[element] = value;
                        break;
                      }
                    }
                  }
                } catch (err){}
              }
            }
          }
        }

        function onDeviceReady() {
          self.get('controller').incrementProperty('hideSplashCount');
          var iOS7 = window.device
            && window.device.platform
            && window.device.platform.toLowerCase() == "ios"
            && parseFloat(window.device.version) >= 7.0;
          if (iOS7) {
            $('.ios7statusBar').css('padding-top', '20px');
            //The changecss functional actually changes the css class definition as opposed tothe jquery selector which only changes elements that currently match the selector
            changeCSS('.modal', 'padding-top', '20px')

          }
        }

      }

    }, Ember.TargetActionSupport);

    return ApplicationView;
  });
define("appkit/views/help",
  [],
  function() {
    "use strict";
    var HelpView = Ember.View.extend({

      didInsertElement: function() {

    //    $.getScript('https://www.pubble.co/resources/javascript/QAInit.js').then(function() {
    //      console.log('after script');
    //      var lpQA = lpQA({
    //        appID:"2295",
    //        genQ: "false",
    //        identifier: "entry2005"
    //      });
    //
    //    })

    //    var script = document.createElement( 'script' );
    //    script.type = 'text/javascript';
    //    script.src = 'https://www.pubble.co/resources/javascript/QAInit.js';
    //    $('body').append(script);
    //      console.log('after script');
          var lpQA = window.lpQA({
            appID:"2295",
            genQ: "false",
            identifier: "entry2005"
          });
      }

    });



    return HelpView;
  });
define("appkit/views/passcode_login",
  [],
  function() {
    "use strict";
    var PassCodeLoginView = Ember.View.extend({

    //  InsertElement: function() {
    //    $('body').css('background-color', '#69ca14');
    //  }

    });

    return PassCodeLoginView;
  });
define("appkit/views/report",
  [],
  function() {
    "use strict";
    var ReportView = Ember.View.extend({

    });

    return ReportView;
  });
define("appkit/views/report/account",
  ["appkit/mixins/equal_box_heights"],
  function(EqualBoxHeights) {
    "use strict";

    var ReportAccountView = Ember.View.extend({

      didInsertElement: function() {
        this.equalizeBoxHeights();
        console.log('yeah');
        this.equalizeBoxHeights('.accountDetailsColumn');
      }

    }, EqualBoxHeights);

    return ReportAccountView;
  });
define("appkit/views/report/accounts",
  ["appkit/mixins/equal_box_heights"],
  function(EqualBoxHeights) {
    "use strict";

    var ReportAccountsView = Ember.View.extend({


      didInsertElement: function() {
        this.equalizeBoxHeights();
      }

    }, EqualBoxHeights);

    return ReportAccountsView;
  });
define("appkit/views/report/summary",
  ["appkit/mixins/equal_box_heights"],
  function(EqualBoxHeights) {
    "use strict";

    var SummaryView = Ember.View.extend({

      didInsertElement: function () {
        this.equalizeBoxHeights();
        this.get('controller.controllers.application').incrementProperty('hideSplashCount');
      }

    }, EqualBoxHeights);

    return SummaryView;
  });
//@ sourceMappingURL=app.js.map